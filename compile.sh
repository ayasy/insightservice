#!/bin/bash

# change into project path
CURRENT_PATH=$(cd "$(dirname "$0")"; pwd)
cd $CURRENT_PATH

# enter into build
cd ./build 

# set envirment
export LD_LIBRARY_PATH=./build/:$LD_LIBRARY_PATH

# build target
if [ $# != 0 ]
then
    if [ $1 == "--rebuild" ]
    then
        make clean
    fi
fi

# cmake
if [ ! -f "MakeFile" ]; then
  cmake ..
fi

# build
make -j1
