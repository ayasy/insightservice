#!/bin/bash


# change into project path
CURRENT_PATH=$(cd "$(dirname "$0")"; pwd)
cd $CURRENT_PATH

# set envirment
export LD_LIBRARY_PATH=./build/:$LD_LIBRARY_PATH
ulimit -c unlimited

# enter into build
cd build

if [ ! -d logs ];then
  mkdir logs
fi


# run target
if [ $# == 0 ]
then
    ./InsightService
    exit
fi

if [ $1 == "--stop" ]
then
    kill -9 $(pidof ./InsightService)
    exit
fi

if [ $1 == "--test" ]
then
    ./InsightService $1
    exit
fi

CUR_DATE="`date +%Y-%m-%d`"
if [ $1 == "--nohup" ]
then
    nohup ./InsightService >>"logs/$CUR_DATE.log" 2>&1 &
    exit
fi

if [ $1 == "--playback" ]
then
    nohup ./InsightService --test 5 >>"logs/$CUR_DATE.log" 2>&1 &
    exit
fi


if [ $1 == "--optimize" ]
then
    nohup ./InsightService --test 7 >>"logs/$CUR_DATE.log" 2>&1 &
    exit
fi

if [ $1 == "--recursive" ]
then
    nohup ./InsightService --test 8 >>"logs/$CUR_DATE.log" 2>&1 &
    exit
fi
