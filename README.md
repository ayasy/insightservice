Insight Service
=========================================================

## Instruction

    A Project to Update Market Database from Insight Service.
    Works On Linux Only.

#### Compile
    
    ./compile.sh

#### Execute

    ./run.sh
    ./run.sh --test

#### Execute Nohup

    ./run.sh --nohup
    ./run.sh --stop

#### Execute Playback Nohup

    ./run.sh --playback
    ./run.sh --stop
    
#### Execute Optimize Nohup

    ./run.sh --optimize
    ./run.sh --stop
    
#### Config
    
    ./build/config/config.ini
    ./build/config/playback.ini

#### Logs

    ./build/logs/YY-mm-dd.log

#### Origin Data Folder

    ./build/export_folders/

#### GitLab

    git clone git@bitbucket.org:ayasy/insightservice.git

## Copyright

    PRODUCT: 易股资产
    AUTHOR: 赵胜
    EMAIL: zhaosheng@totquant.com
