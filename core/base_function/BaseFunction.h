#pragma once

#include <string>
#include <cassert>
#include <iostream>
#include <memory>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <chrono>
#include <ctime>
#include  <stdlib.h>

#if defined(WIN32) || defined(_WIN32)
#include <windows.h> 
#include <direct.h>
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h> 
#include <unistd.h> 
#endif

#include "base_define.h"
#include "mdc_client_factory.h"
#include "client_interface.h"
#include "message_handle.h"

using namespace com::htsc::mdc::gateway;
using namespace com::htsc::mdc::model;
using namespace com::htsc::mdc::insight::model;

#if defined(_MSC_VER) && (_MSC_VER < 1900)
#define snprintf _snprintf
#endif

void msleep(int ms);
std::string get_security_type_name(const ESecurityType& securityType);
std::string get_data_type_name(const EMarketDataType& type);
bool create_folder(const std::string& folder_path);
bool folder_exist(const std::string& dir);
void save_debug_string(std::string base_forder, std::string data_type,
	std::string security_type, std::string SecurityId, const std::string& debug_string);

