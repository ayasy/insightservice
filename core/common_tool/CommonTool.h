#pragma once

#include <stdint.h>
#include <string>
#include <ctime>

std::string TransferTime(const int32_t&);
std::string TransferDate(const int32_t&);
std::time_t TransferDate(const char*);

std::string TransferStatusSH(const std::string&);
std::string TransferStatusSZ(const std::string&);

std::string TransferMarketTypeSZ(const int&);
std::string TransferSecurityTypeSZ(const int&);

int32_t TransferOrderTypeSZ(const int&);
int32_t TransferOrderSideSZ(const int&);
char TransferOrderFlagSH(const int&);

bool JudgeTradeTime(const int32_t&);
bool JudgeActiveTime(const int32_t&);
bool JudgeStockInfo(const std::string&);

std::string GetCurrentDate();
