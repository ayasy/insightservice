#pragma once

#include <mutex>
#include <stack>


template<class T, std::size_t S>
class MemPool
{
public:
	MemPool()
    {
	    MAX_SIZE = S;
        SIZE = sizeof(T);
    }
	
	MemPool(const MemPool &) = delete;
	MemPool &operator=(const MemPool &) = delete;

    ~MemPool() {
	    clearall();
    }

public:
	T* get()
    {
    	T* node = nullptr;
    	
        if (0 == SIZE)
    		return node;
        {
    		std::lock_guard<std::mutex> lock(m_mutex);
    		if (!m_data.empty()) {
                node = m_data.top();
                m_data.pop();
                return node;
    		}
        }
    	node = (T*)malloc(SIZE);
        
        return node;
    }

	void del(T* node)
    {
    	if (!node)
    		return;
    
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            if (m_data.size() <= MAX_SIZE) { 
    	        m_data.push(node);
                return;
            }
        }
        free(node);
        node = nullptr;
    }

    std::size_t size() {
    	std::lock_guard<std::mutex> lock(m_mutex);
        return m_data.size();
    }


private:
	void clearall()
    {
        T* node = nullptr;
    	std::lock_guard<std::mutex> lock(m_mutex);
        while(!m_data.empty()) {
            node = m_data.top();
            m_data.pop();
            if (node) { 
                free(node);
                node = nullptr;
            }
        }
    }

private:
	std::stack<T*> m_data;
	std::mutex m_mutex;
	unsigned MAX_SIZE;
    std::size_t SIZE;
};

