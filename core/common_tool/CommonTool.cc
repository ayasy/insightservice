#include "core/common_tool/CommonTool.h"
#include <stdio.h>
#include <string>


std::string TransferTime(const int32_t& time)
{
    char t_time[13] = {""};
    std::string t = std::to_string(time);
    if (t.size() < 9) 
        t = std::string(9 - t.size(), '0') + t;
    snprintf(t_time, 13, "%c%c:%c%c:%c%c.%c%c%c", t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7], t[8]);
    return std::string(t_time);
}

std::string TransferDate(const int32_t& date)
{
    char d_date[11] = {""};
    std::string d = std::to_string(date);
    if (d.size() < 8)
        return std::string(d_date);
    snprintf(d_date, 11, "%c%c%c%c-%c%c-%c%c", d[0], d[1], d[2], d[3], d[4], d[5], d[6], d[7]);
    return std::string(d_date);
}

std::time_t TransferDate(const char* date)
{
    tm tm_;  
    time_t t_;  
    char buf[20];
    snprintf(buf, 20, "%s 12:00:00", date);
    strptime(buf, "%Y-%m-%d %H:%M:%S", &tm_); 
    tm_.tm_isdst = -1;  
    t_  = mktime(&tm_);  
    return t_;  
}

int32_t TransferOrderSideSZ(const int& side) {
    int32_t order_side = 0;
    switch(side) {
    case 1:
        order_side = 49;
        break;
    case 2:
        order_side = 50;
        break;
    case 3:
        order_side = 71;
        break;
    case 4:
        order_side = 70;
        break;
    }
    return order_side;
}

char TransferOrderFlagSH(const int& order_flag) {
    char flag = 'N';
    switch(order_flag) {
    case 1:
        flag = 'B';
        break;
    case 2:
        flag = 'S';
        break;
    }
    return flag;
}


std::string TransferStatusSH(const std::string& status)
{
    std::string status_sh("EDNTR");
    if (status.empty())
        return status_sh;
    switch(status[0]) 
    {
        case '0':
            status_sh = "START";
            break;
        case '1':
            status_sh = "OCALL";
            break;
        case '3':
            status_sh = "TRADE";
            break;
        case '5':
            status_sh = "CCALL";
            break;
        case '6':
            status_sh = "CLOSE";
            break;
        case '8':
            status_sh = "SUSP";
            break;
    }
    return status_sh;
}

std::string TransferStatusSZ(const std::string& status)
{
    std::string status_sz = "E0";
    if (status.empty())
        return status_sz;
    switch(status[0]) 
    {
        case '0':
            status_sz[0] = 'S';
            break;
        case '1':
            status_sz[0] = 'O';
            break;
        case '3':
            status_sz[0] = 'T';
            break;
        case '4':
            status_sz[0] = 'B';
            break;
        case '5':
            status_sz[0] = 'C';
            break;
        case '6':
            break;
        case '7':
            status_sz[0] = 'A';
            break;
        case '8':
            status_sz[0] = 'H';
            break;
        case '9':
            status_sz[0] = 'V';
            break;
    }

    return std::string(status_sz);
}



std::string TransferMarketTypeSZ(const int& type)
{
    std::string stream_id = "0";
    switch(type) {
        case 5:
            stream_id = "021";
            break;
        case 2:
        case 3:
        case 4:
        case 13:
            stream_id = "011";
            break;
    }
    
    return stream_id;
}

std::string TransferSecurityTypeSZ(const int& type)
{
    std::string stream_id = "0";
    switch(type) {
        case 5:
            stream_id = "020";
            break;
        case 2:
        case 3:
        case 4:
        case 13:
            stream_id = "010";
            break;
    }
    
    return stream_id;
}

int32_t TransferOrderTypeSZ(const int& type) {
    int32_t order_type = 0;
    switch (type)
    {
        case 1:
            order_type = 49;
            break;
        case 2:
            order_type = 50;
            break;
        case 3:
            order_type = 85;
            break;
        case 10:
            order_type = 52;
            break;
    }

    return order_type;
}


bool JudgeTradeTime(const int32_t& time) {
    if ((time >= 91500000 && time <= 113100000) ||
        (time >= 130000000 && time <= 153100000))
        return true;
    return false;
}

bool JudgeActiveTime(const int32_t& time) {
    if ((time >= 91000000 && time <= 113500000) ||
        (time >= 125500000 && time <= 150500000))
        return true;
    return false;
}


bool JudgeStockInfo(const std::string& stock) {
    if ((stock[0] == '0' && stock[1] == '0') ||
        (stock[0] == '3' && stock[1] == '0') ||
        stock[0] == '6')
        return true;
    return false;
}

std::string GetCurrentDate() {
    char date[9];
    std::time_t time;
    struct std::tm* p;
    std::time(&time);
    p = std::localtime(&time);
    snprintf(date, 9, "%04d%02d%02d", p->tm_year + 1900, p->tm_mon + 1, p->tm_mday);
    return std::string(date);
}
