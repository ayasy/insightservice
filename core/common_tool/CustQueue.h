#pragma once

#include <mutex>
#include <condition_variable>
#include <queue>
#include <string>

template<class T>
class CustQueue
{
public:
	CustQueue(const std::string name = "")
    {
	    m_name = name;
    }

	CustQueue(const CustQueue &) = delete;
	CustQueue &operator=(const CustQueue &) = delete;

	~CustQueue() {}

public:
	T get()
    {
    	T data;
    
    	{
    		std::unique_lock<std::mutex> ul(m_mutex);
    		while (m_qdata.empty())
    			m_cv.wait(ul);
    		data = m_qdata.front();
    		m_qdata.pop();
    	}
    
    	return data;
    }
    
    void set(T data) {
	    {
		    std::lock_guard<std::mutex> lock(m_mutex);
		    m_qdata.push(data);
	    }
	    m_cv.notify_one();
    }

	std::string name()
    {
	    return m_name;
    }

private:
	std::string m_name;
	std::queue<T> m_qdata;
	std::mutex m_mutex;
	std::condition_variable m_cv;
};

