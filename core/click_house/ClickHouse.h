#pragma once
#include "core/insight_adapter/AdapterRegistry.h"
#include "core/common_tool/CommonTool.h"
#include "core/insight_handle/InsightHandle.h"
#include <clickhouse/client.h>
#include <clickhouse/error_codes.h>
#include <clickhouse/types/type_parser.h>
#include <ctime>
#include <memory>
#include <iostream>
#include <atomic>

// test with out database flushing
//#define DATA_TEST

// insight queue size
static const int MARKET_QUEUE_SIZE = 10;
static const int ORDER_QUEUE_SIZE = 50;
// channels
static const int CLICK_HOUSE_PIPE = 8;
// cache size
static const unsigned MULTIPLE = 5;

enum ClickHouseError {
    ERROR_SUCCESS,
    ERROR_DISCONECTED,
    ERROR_NONENTITY,
    ERROR_ILLEGAL,
    ERROR_FAILURE,
};

typedef clickhouse::ColumnLowCardinalityT<clickhouse::ColumnFixedString> CK_LOW_FIXED_STRING;

// PIPI 0-INSIGHT_MARKET_SH for MarketSH
// PIPI 1-INSIGHT_MARKET_SZ for MarketSZ
// PIPI 2-INSIGHT_ORDER_SZ for OrderSZ
// PIPI 3-INSIGHT_TRADE_SH for TradeSH
// PIPI 4-INSIGHT_TRADE_SZ for TradeSZ
// PIPI 5-INSIGHT_QUEUE_SH for QueueSH
// PIPI 6-INSIGHT_QUEUE_SZ for QueueSZ
// PIPI 7-INSIGHT_ORDER_SH for OrderSH

class ClickHouse
{
public:
    ClickHouse() {}
    ~ClickHouse() {}

    void init();
    
    static ClickHouse* GetInstance(); 

    template<typename T>
    ClickHouseError UpdateStorage(T*);

    template<typename T>
    ClickHouseError CreateStorage();
    
    template<typename T>
    ClickHouseError DumpStorage(T*);
    
    template<typename T>
    ClickHouseError OptimizeStorage();

    void OptimizeToday();
    void CreateCurrentMonth();
    void SetCurrentMonth(const std::string&);

    std::unique_ptr<clickhouse::Client> init_client();

private:

    template<class T, int S>
    static std::vector<std::shared_ptr<T> > InitQueueList() {
        std::vector<std::shared_ptr<T> > list(S, nullptr);
        for (int i = 0; i < S; ++i) 
            list[i] = std::make_shared<T>();
        return list;
    } 
    
    
    std::string get_table_sh_name(const std::string&);
    std::string get_table_sz_name(const std::string&);

    void load_config();

    template<typename T>
    static std::shared_ptr<T> init_column(const std::string& name, clickhouse::Block& block) {
        std::shared_ptr<T> ptr = std::make_shared<T>();
        block.AppendColumn(name, ptr);
        return ptr;
    }
    
    template<typename T>
    static std::shared_ptr<clickhouse::ColumnNullable> init_column_nullable(const std::string& name, clickhouse::Block& block) {
        std::shared_ptr<T> ptr_t = std::make_shared<T>();
        std::shared_ptr<clickhouse::ColumnUInt8> ptr_flag = std::make_shared<clickhouse::ColumnUInt8>();
        std::shared_ptr<clickhouse::ColumnNullable> ptr = std::make_shared<clickhouse::ColumnNullable>(ptr_t, ptr_flag);
        block.AppendColumn(name, ptr);
        return ptr;
    }
    
    template<typename T>
    static std::vector<std::shared_ptr<clickhouse::ColumnNullable> > init_queue_columns_nullable(const std::string& name, std::size_t size, clickhouse::Block& block) {
        std::vector<std::shared_ptr<clickhouse::ColumnNullable> > ptrs(size);
        for (uint64_t i = 0; i < size; ++i) {
            std::shared_ptr<T> ptr_t = std::make_shared<T>();
            std::shared_ptr<clickhouse::ColumnUInt8> ptr_flag = std::make_shared<clickhouse::ColumnUInt8>();
            ptrs[i] = std::make_shared<clickhouse::ColumnNullable>(ptr_t, ptr_flag);
            block.AppendColumn(name + std::to_string(i + 1), ptrs[i]);
        }
        return ptrs;
    }
    
    static std::shared_ptr<CK_LOW_FIXED_STRING> init_fixed_column(const std::string&, std::size_t, clickhouse::Block&);
    
    static std::shared_ptr<clickhouse::ColumnDate> init_date_column(const std::string&, clickhouse::Block&);

    static void clear_block(clickhouse::Block&);

private:
    std::unique_ptr<clickhouse::Client> client[CLICK_HOUSE_PIPE];
    // for level2 constant
    // coefficient
    int COEFFICIENT;
    // clickhouse queue size
    int MARKET_SH_BATCH;
    int MARKET_SZ_BATCH;
    int ORDER_SZ_BATCH;
    int TRADE_SH_BATCH;
    int TRADE_SZ_BATCH;
    int QUEUE_SH_BATCH;
    int QUEUE_SZ_BATCH;
    int ORDER_SH_BATCH;
    // for clickhouse
    int port;
    char month[7];
    std::string ip;
    std::string username;
    std::string password;
    // prefix
    std::string DATABASE_PREFIX;
    std::string TABLE_SH_PREFIX;
    std::string TABLE_SZ_PREFIX;
    // table_name
    std::string market_sh_table;
    std::string market_sz_table;
    std::string order_sh_table;
    std::string trade_sh_table;
    std::string trade_sz_table;
    std::string queue_sh_table;
    std::string queue_sz_table;
    std::string order_sz_table;
};


#define DEFINE_COLUMN(BLOCK, TYPE, NAME) \
    static std::shared_ptr<TYPE> NAME = init_column<TYPE>(#NAME, BLOCK);    

#define DEFINE_COLUMN_NULLABLE(BLOCK, TYPE, NAME) \
    static std::shared_ptr<clickhouse::ColumnNullable> NAME = init_column_nullable<TYPE>(#NAME, BLOCK);    

#define DEFINE_QUEUE_COLUMNS_NULLABLE(BLOCK, TYPE, NAME, SIZE)   \
    static std::vector<std::shared_ptr<clickhouse::ColumnNullable> > NAME = init_queue_columns_nullable<TYPE>(#NAME, SIZE, BLOCK);    

#define DEFINE_FIXED_COLUMN(BLOCK, NAME, LENGTH) \
    static std::shared_ptr<CK_LOW_FIXED_STRING> NAME = init_fixed_column(#NAME, LENGTH, BLOCK);    

#define DEFINE_DATE_COLUMN(BLOCK, NAME) \
    static std::shared_ptr<clickhouse::ColumnDate> NAME = init_date_column(#NAME, BLOCK);   



#define APPEND_COLUMN(TYPE, NAME, PTR) \
    NAME->Append(PTR->NAME);    

#define APPEND_COLUMN_NULLABLE(TYPE, NAME, PTR) \
    NAME->Nested()->As<TYPE>()->Append(PTR->NAME);    \
    NAME->Nulls()->As<clickhouse::ColumnUInt8>()->Append((fabs(PTR->NAME - PLACE_HOLDER) < TINY ) ? 1 : 0); 

#define APPEND_QUEUE_COLUMNS_NULLABLE(TYPE, NAME, SIZE, PTR)   \
    for (int i = 0; i < SIZE; ++i) {    \
        NAME[i]->Nested()->As<TYPE>()->Append(PTR->NAME[i]); \
        NAME[i]->Nulls()->As<clickhouse::ColumnUInt8>()->Append((fabs(PTR->NAME[i] - PLACE_HOLDER) < TINY) ? 1 : 0); \
    }

#define APPEND_FIXED_COLUMN(NAME, LENGTH, PTR) \
    NAME->Append(PTR->NAME);    

#define APPEND_DATE_COLUMN(NAME, PTR) \
    NAME->Append(TransferDate(PTR->NAME));    

