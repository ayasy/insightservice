#include "core/click_house/ClickHouse.h"
#include "core/datayes_weave/QueueSZ.h"


template<>
ClickHouseError ClickHouse::UpdateStorage<QueueSZ>(QueueSZ_t ptr) 
{
    static int count = 0;;
    static clickhouse::Block block;
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, Price);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt64, Volume);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnUInt64, NumOrders);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnUInt64, NoOrders);
    DEFINE_COLUMN(block, clickhouse::ColumnUInt64, SeqNo);
    DEFINE_QUEUE_COLUMNS_NULLABLE(block, clickhouse::ColumnUInt64, OrderQty, ORDER_QUEUE_SIZE);
    DEFINE_COLUMN(block, clickhouse::ColumnUInt8, ImageStatus);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnUInt8, NoPriceLevel);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnUInt8, PrcLvlOperator);
    DEFINE_COLUMN(block, clickhouse::ColumnString, DataTimeStamp);
    DEFINE_COLUMN(block, clickhouse::ColumnString, LocalTime);
    DEFINE_DATE_COLUMN(block, Date);
    DEFINE_FIXED_COLUMN(block, SecurityID, 6);
    DEFINE_FIXED_COLUMN(block, PrefixSecurityID, 2);
    DEFINE_FIXED_COLUMN(block, Side, 1);
    
    if (ptr) {
        
        if (ptr->NoOrders <= 0)
            return ClickHouseError::ERROR_SUCCESS;

        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, Price, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt64, Volume, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnUInt64, NumOrders, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnUInt64, NoOrders, ptr);
        APPEND_COLUMN(clickhouse::ColumnUInt64, SeqNo, ptr);
        APPEND_QUEUE_COLUMNS_NULLABLE(clickhouse::ColumnUInt64, OrderQty, ORDER_QUEUE_SIZE, ptr);
        APPEND_COLUMN(clickhouse::ColumnUInt8, ImageStatus, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnUInt8, NoPriceLevel, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnUInt8, PrcLvlOperator, ptr);
        APPEND_COLUMN(clickhouse::ColumnString, DataTimeStamp, ptr);
        APPEND_COLUMN(clickhouse::ColumnString, LocalTime, ptr);
        APPEND_DATE_COLUMN(Date, ptr);
        APPEND_FIXED_COLUMN(SecurityID, 6, ptr);
        APPEND_FIXED_COLUMN(PrefixSecurityID, 2, ptr);
        APPEND_FIXED_COLUMN(Side, 1, ptr);
        
        count++;

        // When the threshold is reached, it is stored in the database
        if (count < QUEUE_SZ_BATCH) 
            return ClickHouseError::ERROR_SUCCESS;

    }

    // nullptr sent periodically are signals that are stored in the database
    if (count <= 0)
        return ClickHouseError::ERROR_SUCCESS;

    block.RefreshRowCount();
    debug_print("[%d,%d] row-column will be stored into table: %s", count, block.GetColumnCount(), queue_sz_table.c_str());
    count = 0;

    try {
#ifndef DATA_TEST
        client[INSIGHT_QUEUE_SZ]->Insert(queue_sz_table, block);
#endif
        clear_block(block);
    } 
    catch (std::exception& e) {
        warning_print("%s: %s", __FUNCTION__, e.what());
        client[INSIGHT_QUEUE_SZ] = ClickHouse::GetInstance()->init_client();
        if (block.GetRowCount() > MULTIPLE * QUEUE_SZ_BATCH) {
            clear_block(block);
            error_print("Buffer overflow, discarding data sent to table: %s", queue_sz_table.c_str());
        }
    }

    return ClickHouseError::ERROR_SUCCESS;
}

template<>
ClickHouseError ClickHouse::OptimizeStorage<QueueSZ>() 
{
    std::string optimize = "optimize table " + queue_sz_table
        + " partition '" + GetCurrentDate() + "' final";
    client[INSIGHT_QUEUE_SZ]->Select(optimize, nullptr);
    debug_print("Execute optimization on table QueueSZ success...");
    return ClickHouseError::ERROR_SUCCESS;
}

template<>
ClickHouseError ClickHouse::CreateStorage<QueueSZ>() 
{
    std::string create = "CREATE TABLE IF NOT EXISTS " + queue_sz_table + 
    " (`Date` Date, `PrefixSecurityID` LowCardinality(FixedString(2)), `DataTimeStamp` String, `SecurityID` LowCardinality(FixedString(6)), \
    `ImageStatus` UInt8, `Side` LowCardinality(FixedString(1)), `NoPriceLevel` Nullable(UInt8), `PrcLvlOperator` Nullable(UInt8), \
    `Price` Nullable(Float64), `Volume` Nullable(Int64), `NumOrders` Nullable(UInt64), `NoOrders` Nullable(UInt64), \
    `OrderQty1` Nullable(UInt64), `OrderQty2` Nullable(UInt64), `OrderQty3` Nullable(UInt64), `OrderQty4` Nullable(UInt64), \
    `OrderQty5` Nullable(UInt64), `OrderQty6` Nullable(UInt64), `OrderQty7` Nullable(UInt64), `OrderQty8` Nullable(UInt64), \
    `OrderQty9` Nullable(UInt64), `OrderQty10` Nullable(UInt64), `OrderQty11` Nullable(UInt64), `OrderQty12` Nullable(UInt64), \
    `OrderQty13` Nullable(UInt64), `OrderQty14` Nullable(UInt64), `OrderQty15` Nullable(UInt64), `OrderQty16` Nullable(UInt64), \
    `OrderQty17` Nullable(UInt64), `OrderQty18` Nullable(UInt64), `OrderQty19` Nullable(UInt64), `OrderQty20` Nullable(UInt64), \
    `OrderQty21` Nullable(UInt64), `OrderQty22` Nullable(UInt64), `OrderQty23` Nullable(UInt64), `OrderQty24` Nullable(UInt64), \
    `OrderQty25` Nullable(UInt64), `OrderQty26` Nullable(UInt64), `OrderQty27` Nullable(UInt64), `OrderQty28` Nullable(UInt64), \
    `OrderQty29` Nullable(UInt64), `OrderQty30` Nullable(UInt64), `OrderQty31` Nullable(UInt64), `OrderQty32` Nullable(UInt64), \
    `OrderQty33` Nullable(UInt64), `OrderQty34` Nullable(UInt64), `OrderQty35` Nullable(UInt64), `OrderQty36` Nullable(UInt64), \
    `OrderQty37` Nullable(UInt64), `OrderQty38` Nullable(UInt64), `OrderQty39` Nullable(UInt64), `OrderQty40` Nullable(UInt64), \
    `OrderQty41` Nullable(UInt64), `OrderQty42` Nullable(UInt64), `OrderQty43` Nullable(UInt64), `OrderQty44` Nullable(UInt64), \
    `OrderQty45` Nullable(UInt64), `OrderQty46` Nullable(UInt64), `OrderQty47` Nullable(UInt64), `OrderQty48` Nullable(UInt64), \
    `OrderQty49` Nullable(UInt64), `OrderQty50` Nullable(UInt64), `LocalTime` String, `SeqNo` UInt64) \
    ENGINE = ReplacingMergeTree PARTITION BY toYYYYMMDD(Date) ORDER BY (Side, PrefixSecurityID, DataTimeStamp, SecurityID) SETTINGS index_granularity = 8192";

    client[INSIGHT_QUEUE_SZ]->Select(create, nullptr);
    debug_print("Execute creation on table QueueSZ success...");
    return ClickHouseError::ERROR_SUCCESS;
}


template<>
ClickHouseError ClickHouse::DumpStorage<QueueSZ>(const QueueSZ_t ptr) 
{
    return ClickHouseError::ERROR_SUCCESS;
}

