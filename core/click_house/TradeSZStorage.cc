#include "core/click_house/ClickHouse.h"
#include "core/datayes_weave/TradeSZ.h"


template<>
ClickHouseError ClickHouse::UpdateStorage<TradeSZ>(TradeSZ_t ptr) 
{
    static int count = 0;;
    static clickhouse::Block block;
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, LastPx);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt64, ApplSeqNum);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt64, BidApplSeqNum);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt64, OfferApplSeqNum);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt64, LastQty);
    DEFINE_COLUMN(block, clickhouse::ColumnInt64, TradeIndex);
    DEFINE_COLUMN(block, clickhouse::ColumnUInt64, SeqNo);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt32, ChannelNo);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt32, ExecType);
    DEFINE_COLUMN(block, clickhouse::ColumnString, TransactTime);
    DEFINE_COLUMN(block, clickhouse::ColumnString, LocalTime);
    DEFINE_DATE_COLUMN(block, Date);
    DEFINE_FIXED_COLUMN(block, SecurityID, 6);
    DEFINE_FIXED_COLUMN(block, MDStreamID, 3);
    DEFINE_FIXED_COLUMN(block, SecurityIDSource, 3);
    DEFINE_FIXED_COLUMN(block, PrefixSecurityID, 2);
   
    if (ptr) {
        
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, LastPx, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt64, ApplSeqNum, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt64, BidApplSeqNum, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt64, OfferApplSeqNum, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt64, LastQty, ptr);
        APPEND_COLUMN(clickhouse::ColumnInt64, TradeIndex, ptr);
        APPEND_COLUMN(clickhouse::ColumnUInt64, SeqNo, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt32, ChannelNo, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt32, ExecType, ptr);
        APPEND_COLUMN(clickhouse::ColumnString, TransactTime, ptr);
        APPEND_COLUMN(clickhouse::ColumnString, LocalTime, ptr);
        APPEND_DATE_COLUMN(Date, ptr);
        APPEND_FIXED_COLUMN(SecurityID, 6, ptr);
        APPEND_FIXED_COLUMN(MDStreamID, 3, ptr);
        APPEND_FIXED_COLUMN(SecurityIDSource, 3, ptr);
        APPEND_FIXED_COLUMN(PrefixSecurityID, 2, ptr);
        
        count++;

        // When the threshold is reached, it is stored in the database
        if (count < TRADE_SZ_BATCH) 
            return ClickHouseError::ERROR_SUCCESS;

    }

    // nullptr sent periodically are signals that are stored in the database
    if (count <= 0)
        return ClickHouseError::ERROR_SUCCESS;

    block.RefreshRowCount();
    debug_print("[%d,%d] row-column will be stored into table: %s", count, block.GetColumnCount(), trade_sz_table.c_str());
    count = 0;
    
    try {
#ifndef DATA_TEST
        client[INSIGHT_TRADE_SZ]->Insert(trade_sz_table, block);
#endif
        clear_block(block);
    } 
    catch (std::exception& e) {
        warning_print("%s: %s", __FUNCTION__, e.what());
        client[INSIGHT_TRADE_SZ] = ClickHouse::GetInstance()->init_client();
        if (block.GetRowCount() > MULTIPLE * TRADE_SZ_BATCH) {
            clear_block(block);
            error_print("Buffer overflow, discarding data sent to table: %s", trade_sz_table.c_str());
        }
    }
    
    return ClickHouseError::ERROR_SUCCESS;
}

template<>
ClickHouseError ClickHouse::OptimizeStorage<TradeSZ>() 
{
    std::string optimize = "optimize table " + trade_sz_table
        + " partition '" + GetCurrentDate() + "' final";
    client[INSIGHT_TRADE_SZ]->Select(optimize, nullptr);
    debug_print("Execute optimization on table TradeSZ success...");
    return ClickHouseError::ERROR_SUCCESS;
}

template<>
ClickHouseError ClickHouse::CreateStorage<TradeSZ>() 
{
    std::string create = "CREATE TABLE IF NOT EXISTS " + trade_sz_table  + 
    " (`Date` Date, `PrefixSecurityID` LowCardinality(FixedString(2)), `TradeIndex` Int64, `ChannelNo` Nullable(Int32), \
    `ApplSeqNum` Nullable(Int64), `MDStreamID` LowCardinality(FixedString(3)), `BidApplSeqNum` Nullable(Int64), \
    `OfferApplSeqNum` Nullable(Int64), `SecurityID` LowCardinality(FixedString(6)), `SecurityIDSource` LowCardinality(FixedString(3)), \
    `LastPx` Nullable(Float64), `LastQty` Nullable(Int64), `ExecType` Nullable(Int32), `TransactTime` String, `LocalTime` String, `SeqNo` UInt64) \
    ENGINE = ReplacingMergeTree PARTITION BY toYYYYMMDD(Date) ORDER BY (PrefixSecurityID, TransactTime, SecurityID, TradeIndex) SETTINGS index_granularity = 8192";

    client[INSIGHT_TRADE_SZ]->Select(create, nullptr);
    debug_print("Execute creation on table TradeSZ success...");
    return ClickHouseError::ERROR_SUCCESS;
}


template<>
ClickHouseError ClickHouse::DumpStorage<TradeSZ>(const TradeSZ_t ptr) 
{
    return ClickHouseError::ERROR_SUCCESS;
}

