#include "core/click_house/ClickHouse.h"
#include "core/datayes_weave/MarketSZ.h"


template<>
ClickHouseError ClickHouse::UpdateStorage<MarketSZ>(MarketSZ_t ptr) 
{
    static int count = 0;
    static clickhouse::Block block;
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, PreCloPrice);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, Turnover);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, LastPrice);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, OpenPrice);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, HighPrice);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, LowPrice);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, DifPrice1);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, DifPrice2);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, PE1);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, PE2);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, WeightedAvgBidPx);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, WeightedAvgOfferPx);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, HighLimitPrice);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, LowLimitPrice);
    DEFINE_QUEUE_COLUMNS_NULLABLE(block, clickhouse::ColumnFloat64, AskPrice, MARKET_QUEUE_SIZE);
    DEFINE_QUEUE_COLUMNS_NULLABLE(block, clickhouse::ColumnFloat64, BidPrice, MARKET_QUEUE_SIZE);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt64, TurnNum);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt64, Volume);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt64, TotalBidQty);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt64, TotalOfferQty);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt64, OpenInt);
    DEFINE_QUEUE_COLUMNS_NULLABLE(block, clickhouse::ColumnInt64, AskVolume, MARKET_QUEUE_SIZE);
    DEFINE_QUEUE_COLUMNS_NULLABLE(block, clickhouse::ColumnInt64, BidVolume, MARKET_QUEUE_SIZE);
    DEFINE_COLUMN(block, clickhouse::ColumnUInt64, SeqNo);
    DEFINE_QUEUE_COLUMNS_NULLABLE(block, clickhouse::ColumnUInt32, NumOrdersB, MARKET_QUEUE_SIZE);
    DEFINE_QUEUE_COLUMNS_NULLABLE(block, clickhouse::ColumnUInt32, NumOrdersS, MARKET_QUEUE_SIZE);
    DEFINE_COLUMN(block, clickhouse::ColumnString, UpdateTime);
    DEFINE_COLUMN(block, clickhouse::ColumnString, LocalTime);
    DEFINE_DATE_COLUMN(block, Date);
    DEFINE_FIXED_COLUMN(block, SecurityID, 6);
    DEFINE_FIXED_COLUMN(block, SecurityIDSource, 3);
    DEFINE_FIXED_COLUMN(block, MDStreamID, 3);
    DEFINE_FIXED_COLUMN(block, PrefixSecurityID, 2);
    DEFINE_COLUMN(block, clickhouse::ColumnLowCardinalityT<clickhouse::ColumnString>, TradingPhaseCode);
    
    if (ptr) {

        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, PreCloPrice, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, Turnover, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, LastPrice, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, OpenPrice, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, HighPrice, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, LowPrice, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, DifPrice1, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, DifPrice2, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, PE1, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, PE2, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, WeightedAvgBidPx, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, WeightedAvgOfferPx, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, HighLimitPrice, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, LowLimitPrice, ptr);
        APPEND_QUEUE_COLUMNS_NULLABLE(clickhouse::ColumnFloat64, AskPrice, MARKET_QUEUE_SIZE, ptr);
        APPEND_QUEUE_COLUMNS_NULLABLE(clickhouse::ColumnFloat64, BidPrice, MARKET_QUEUE_SIZE, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt64, TurnNum, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt64, Volume, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt64, TotalBidQty, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt64, TotalOfferQty, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt64, OpenInt, ptr);
        APPEND_QUEUE_COLUMNS_NULLABLE(clickhouse::ColumnInt64, AskVolume, MARKET_QUEUE_SIZE, ptr);
        APPEND_QUEUE_COLUMNS_NULLABLE(clickhouse::ColumnInt64, BidVolume, MARKET_QUEUE_SIZE, ptr);
        APPEND_COLUMN(clickhouse::ColumnUInt64, SeqNo, ptr);
        APPEND_QUEUE_COLUMNS_NULLABLE(clickhouse::ColumnUInt32, NumOrdersB, MARKET_QUEUE_SIZE, ptr);
        APPEND_QUEUE_COLUMNS_NULLABLE(clickhouse::ColumnUInt32, NumOrdersS, MARKET_QUEUE_SIZE, ptr);
        APPEND_COLUMN(clickhouse::ColumnString, UpdateTime, ptr);
        APPEND_COLUMN(clickhouse::ColumnString, LocalTime, ptr);
        APPEND_DATE_COLUMN(Date, ptr);
        APPEND_FIXED_COLUMN(SecurityID, 6, ptr);
        APPEND_FIXED_COLUMN(SecurityIDSource, 3, ptr);
        APPEND_FIXED_COLUMN(MDStreamID, 3, ptr);
        APPEND_FIXED_COLUMN(PrefixSecurityID, 2, ptr);
        APPEND_COLUMN(clickhouse::ColumnLowCardinalityT<clickhouse::ColumnString>, TradingPhaseCode, ptr);

        count++;
        
        // When the threshold is reached, it is stored in the database
        if (count < MARKET_SZ_BATCH) 
            return ClickHouseError::ERROR_SUCCESS;
    }
    
    // nullptr sent periodically are signals that are stored in the database
    if (count <= 0)
        return ClickHouseError::ERROR_SUCCESS;

    block.RefreshRowCount();
    debug_print("[%d,%d] row-column will be stored into table: %s", count, block.GetColumnCount(), market_sz_table.c_str());
    count = 0;

    try {
#ifndef DATA_TEST
        client[INSIGHT_MARKET_SZ]->Insert(market_sz_table, block);
#endif
        clear_block(block);
    } 
    catch (std::exception& e) {
        warning_print("%s: %s", __FUNCTION__, e.what());
        client[INSIGHT_MARKET_SZ] = ClickHouse::GetInstance()->init_client();
        if (block.GetRowCount() > MULTIPLE * MARKET_SZ_BATCH) {
            clear_block(block);
            error_print("Buffer overflow, discarding data sent to table: %s", market_sz_table.c_str());
        }
    }
    
    return ClickHouseError::ERROR_SUCCESS;
}

template<>
ClickHouseError ClickHouse::OptimizeStorage<MarketSZ>() 
{
    std::string optimize = "optimize table " + market_sz_table
        + " partition '" + GetCurrentDate() + "' final";
    client[INSIGHT_MARKET_SZ]->Select(optimize, nullptr);
    debug_print("Execute optimization on table MarketSZ success...");
    return ClickHouseError::ERROR_SUCCESS;
}

template<>
ClickHouseError ClickHouse::CreateStorage<MarketSZ>() 
{
    std::string create = "CREATE TABLE IF NOT EXISTS " + market_sz_table + 
        " (`Date` Date, `PrefixSecurityID` LowCardinality(FixedString(2)), `UpdateTime` String, `MDStreamID` LowCardinality(FixedString(3)), \
        `SecurityID` LowCardinality(FixedString(6)), `SecurityIDSource` LowCardinality(FixedString(3)), `TradingPhaseCode` LowCardinality(String), \
        `PreCloPrice` Nullable(Float64), `TurnNum` Nullable(Int64), `Volume` Nullable(Int64), `Turnover` Nullable(Float64), \
        `LastPrice` Nullable(Float64), `OpenPrice` Nullable(Float64), `HighPrice` Nullable(Float64), `LowPrice` Nullable(Float64), \
        `DifPrice1` Nullable(Float64), `DifPrice2` Nullable(Float64), `PE1` Nullable(Float64), `PE2` Nullable(Float64), \
        `PreCloseIOPV` Nullable(Float64), `IOPV` Nullable(Float64), `TotalBidQty` Nullable(Int64), `WeightedAvgBidPx` Nullable(Float64), \
        `TotalOfferQty` Nullable(Int64), `WeightedAvgOfferPx` Nullable(Float64), `HighLimitPrice` Nullable(Float64), `LowLimitPrice` Nullable(Float64), \
        `OpenInt` Nullable(Int64), `OptPremiumRatio` Nullable(Float64), `AskPrice1` Nullable(Float64), `AskVolume1` Nullable(Int64), \
        `AskPrice2` Nullable(Float64), `AskVolume2` Nullable(Int64), `AskPrice3` Nullable(Float64), `AskVolume3` Nullable(Int64), \
        `AskPrice4` Nullable(Float64), `AskVolume4` Nullable(Int64), `AskPrice5` Nullable(Float64), `AskVolume5` Nullable(Int64), \
        `AskPrice6` Nullable(Float64), `AskVolume6` Nullable(Int64), `AskPrice7` Nullable(Float64), `AskVolume7` Nullable(Int64), \
        `AskPrice8` Nullable(Float64), `AskVolume8` Nullable(Int64), `AskPrice9` Nullable(Float64), `AskVolume9` Nullable(Int64), \
        `AskPrice10` Nullable(Float64), `AskVolume10` Nullable(Int64), `BidPrice1` Nullable(Float64), `BidVolume1` Nullable(Int64), \
        `BidPrice2` Nullable(Float64), `BidVolume2` Nullable(Int64), `BidPrice3` Nullable(Float64), `BidVolume3` Nullable(Int64), \
        `BidPrice4` Nullable(Float64), `BidVolume4` Nullable(Int64), `BidPrice5` Nullable(Float64), `BidVolume5` Nullable(Int64), \
        `BidPrice6` Nullable(Float64), `BidVolume6` Nullable(Int64), `BidPrice7` Nullable(Float64), `BidVolume7` Nullable(Int64), \
        `BidPrice8` Nullable(Float64), `BidVolume8` Nullable(Int64), `BidPrice9` Nullable(Float64), `BidVolume9` Nullable(Int64), \
        `BidPrice10` Nullable(Float64), `BidVolume10` Nullable(Int64), `NumOrdersB1` Nullable(UInt32), `NumOrdersB2` Nullable(UInt32), \
        `NumOrdersB3` Nullable(UInt32), `NumOrdersB4` Nullable(UInt32), `NumOrdersB5` Nullable(UInt32), `NumOrdersB6` Nullable(UInt32), \
        `NumOrdersB7` Nullable(UInt32), `NumOrdersB8` Nullable(UInt32), `NumOrdersB9` Nullable(UInt32), `NumOrdersB10` Nullable(UInt32), \
        `NumOrdersS1` Nullable(UInt32), `NumOrdersS2` Nullable(UInt32), `NumOrdersS3` Nullable(UInt32), `NumOrdersS4` Nullable(UInt32), \
        `NumOrdersS5` Nullable(UInt32), `NumOrdersS6` Nullable(UInt32), `NumOrdersS7` Nullable(UInt32), `NumOrdersS8` Nullable(UInt32), \
        `NumOrdersS9` Nullable(UInt32), `NumOrdersS10` Nullable(UInt32), `LocalTime` String, `SeqNo` UInt64) \
        ENGINE = ReplacingMergeTree PARTITION BY toYYYYMMDD(Date) ORDER BY (PrefixSecurityID, UpdateTime, SecurityID) SETTINGS index_granularity = 8192";
    
    client[INSIGHT_MARKET_SZ]->Select(create, nullptr);
    debug_print("Execute creation on table MarketSZ success...");
    return ClickHouseError::ERROR_SUCCESS;
}


template<>
ClickHouseError ClickHouse::DumpStorage<MarketSZ>(const MarketSZ_t ptr) 
{
    return ClickHouseError::ERROR_SUCCESS;
}

