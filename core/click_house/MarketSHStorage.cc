#include "core/click_house/ClickHouse.h"
#include "core/datayes_weave/MarketSH.h"


template<>
ClickHouseError ClickHouse::UpdateStorage<MarketSH>(MarketSH_t ptr) 
{
    static int count = 0;
    static clickhouse::Block block;
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, TradVolume);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, Turnover);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, TotalBidVol);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, TotalAskVol);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, WiDBuyVol);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, WiDBuyMon);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, WiDSellVol);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, WiDSellMon);
    DEFINE_QUEUE_COLUMNS_NULLABLE(block, clickhouse::ColumnFloat64, AskVolume, MARKET_QUEUE_SIZE);
    DEFINE_QUEUE_COLUMNS_NULLABLE(block, clickhouse::ColumnFloat64, BidVolume, MARKET_QUEUE_SIZE);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt64, MaxBidDur);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt64, MaxSellDur);
    DEFINE_COLUMN(block, clickhouse::ColumnUInt64, SeqNo);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat32, PreCloPrice);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat32, OpenPrice);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat32, HighPrice);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat32, LowPrice);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat32, LastPrice);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat32, ClosePrice);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat32, WAvgBidPri);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat32, WAvgAskPri);
    DEFINE_QUEUE_COLUMNS_NULLABLE(block, clickhouse::ColumnFloat32, AskPrice, MARKET_QUEUE_SIZE);
    DEFINE_QUEUE_COLUMNS_NULLABLE(block, clickhouse::ColumnFloat32, BidPrice, MARKET_QUEUE_SIZE);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnUInt32, WiDBuyNum);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnUInt32, WiDSellNum);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnUInt32, TotBidNum);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnUInt32, TotSellNum);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnUInt32, BidNum);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnUInt32, SellNum);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnUInt32, TradNumber);
    DEFINE_QUEUE_COLUMNS_NULLABLE(block, clickhouse::ColumnUInt32, NumOrdersB, MARKET_QUEUE_SIZE);
    DEFINE_QUEUE_COLUMNS_NULLABLE(block, clickhouse::ColumnUInt32, NumOrdersS, MARKET_QUEUE_SIZE);
    DEFINE_COLUMN(block, clickhouse::ColumnUInt8, ImageStatus);
    DEFINE_COLUMN(block, clickhouse::ColumnString, UpdateTime);
    DEFINE_COLUMN(block, clickhouse::ColumnString, LocalTime);
    DEFINE_DATE_COLUMN(block, Date);
    DEFINE_COLUMN(block, clickhouse::ColumnLowCardinalityT<clickhouse::ColumnString>, InstruStatus);
    DEFINE_FIXED_COLUMN(block, SecurityID, 6);
    DEFINE_FIXED_COLUMN(block, PrefixSecurityID, 1);

    
    if (ptr) {

        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, TradVolume, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, Turnover, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, TotalBidVol, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, TotalAskVol, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, WiDBuyVol, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, WiDBuyMon, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, WiDSellVol, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, WiDSellMon, ptr);
        APPEND_QUEUE_COLUMNS_NULLABLE(clickhouse::ColumnFloat64, AskVolume, MARKET_QUEUE_SIZE, ptr);
        APPEND_QUEUE_COLUMNS_NULLABLE(clickhouse::ColumnFloat64, BidVolume, MARKET_QUEUE_SIZE, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt64, MaxBidDur, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt64, MaxSellDur, ptr);
        APPEND_COLUMN(clickhouse::ColumnUInt64, SeqNo, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat32, PreCloPrice, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat32, OpenPrice, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat32, HighPrice, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat32, LowPrice, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat32, LastPrice, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat32, ClosePrice, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat32, WAvgBidPri, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat32, WAvgAskPri, ptr);
        APPEND_QUEUE_COLUMNS_NULLABLE(clickhouse::ColumnFloat32, AskPrice, MARKET_QUEUE_SIZE, ptr);
        APPEND_QUEUE_COLUMNS_NULLABLE(clickhouse::ColumnFloat32, BidPrice, MARKET_QUEUE_SIZE, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnUInt32, WiDBuyNum, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnUInt32, WiDSellNum, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnUInt32, TotBidNum, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnUInt32, TotSellNum, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnUInt32, BidNum, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnUInt32, SellNum, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnUInt32, TradNumber, ptr);
        APPEND_QUEUE_COLUMNS_NULLABLE(clickhouse::ColumnUInt32, NumOrdersB, MARKET_QUEUE_SIZE, ptr);
        APPEND_QUEUE_COLUMNS_NULLABLE(clickhouse::ColumnUInt32, NumOrdersS, MARKET_QUEUE_SIZE, ptr);
        APPEND_COLUMN(clickhouse::ColumnUInt8, ImageStatus, ptr);
        APPEND_COLUMN(clickhouse::ColumnString, UpdateTime, ptr);
        APPEND_COLUMN(clickhouse::ColumnString, LocalTime, ptr);
        APPEND_DATE_COLUMN(Date, ptr);
        APPEND_COLUMN(clickhouse::ColumnLowCardinalityT<clickhouse::ColumnString>, InstruStatus, ptr);
        APPEND_FIXED_COLUMN(SecurityID, 6, ptr);
        APPEND_FIXED_COLUMN(PrefixSecurityID, 1, ptr);

        count++;

        // When the threshold is reached, it is stored in the database
        if (count < MARKET_SH_BATCH) 
            return ClickHouseError::ERROR_SUCCESS;

    }

    // nullptr sent periodically are signals that are stored in the database
    if (count <= 0)
        return ClickHouseError::ERROR_SUCCESS;

    block.RefreshRowCount();
    debug_print("[%d,%d] row-column will be stored into table: %s", count, block.GetColumnCount(), market_sh_table.c_str());
    count = 0;
    

    try {
#ifndef DATA_TEST
        client[INSIGHT_MARKET_SH]->Insert(market_sh_table, block);
#endif
        clear_block(block);
    } 
    catch (std::exception& e) {
        warning_print("%s: %s", __FUNCTION__, e.what());
        client[INSIGHT_MARKET_SH] = ClickHouse::GetInstance()->init_client();
        if (block.GetRowCount() > MULTIPLE * MARKET_SH_BATCH) {
            clear_block(block);
            error_print("Buffer overflow, discarding data sent to table: %s", market_sh_table.c_str());
        }
    }
    
    return ClickHouseError::ERROR_SUCCESS;
}


template<>
ClickHouseError ClickHouse::CreateStorage<MarketSH>() 
{
    static std::string create = "CREATE TABLE IF NOT EXISTS " + market_sh_table + 
        " (`Date` Date, `PrefixSecurityID` LowCardinality(FixedString(1)), `UpdateTime` String, \
        `SecurityID` LowCardinality(FixedString(6)), `ImageStatus` UInt8, `PreCloPrice` Nullable(Float32), \
        `OpenPrice` Nullable(Float32), `HighPrice` Nullable(Float32), `LowPrice` Nullable(Float32), `LastPrice` Nullable(Float32), \
        `ClosePrice` Nullable(Float32), `InstruStatus` LowCardinality(String), `TradNumber` Nullable(UInt32), `TradVolume` Nullable(Float64), \
        `Turnover` Nullable(Float64), `TotalBidVol` Nullable(Float64), `WAvgBidPri` Nullable(Float32), `AltWAvgBidPri` Nullable(Float32), \
        `TotalAskVol` Nullable(Float64), `WAvgAskPri` Nullable(Float32), `AltWAvgAskPri` Nullable(Float32), `EtfBuyNumber` Nullable(UInt32), \
        `EtfBuyVolume` Nullable(Float64), `EtfBuyMoney` Nullable(Float64), `EtfSellNumber` Nullable(UInt32), `EtfSellVolume` Nullable(Float64), \
        `ETFSellMoney` Nullable(Float64), `YieldToMatu` Nullable(Float32), `TotWarExNum` Nullable(Float64), `WarLowerPri` Nullable(Float64), \
        `WarUpperPri` Nullable(Float64), `WiDBuyNum` Nullable(UInt32), `WiDBuyVol` Nullable(Float64), `WiDBuyMon` Nullable(Float64), \
        `WiDSellNum` Nullable(UInt32), `WiDSellVol` Nullable(Float64), `WiDSellMon` Nullable(Float64), `TotBidNum` Nullable(UInt32), \
        `TotSellNum` Nullable(UInt32), `MaxBidDur` Nullable(Int64), `MaxSellDur` Nullable(Int64), `BidNum` Nullable(UInt32), \
        `SellNum` Nullable(UInt32), `IOPV` Nullable(Float32), `AskPrice1` Nullable(Float32), `AskVolume1` Nullable(Float64), \
        `AskPrice2` Nullable(Float32), `AskVolume2` Nullable(Float64), `AskPrice3` Nullable(Float32), `AskVolume3` Nullable(Float64), \
        `AskPrice4` Nullable(Float32), `AskVolume4` Nullable(Float64), `AskPrice5` Nullable(Float32), `AskVolume5` Nullable(Float64), \
        `AskPrice6` Nullable(Float32), `AskVolume6` Nullable(Float64), `AskPrice7` Nullable(Float32), `AskVolume7` Nullable(Float64), \
        `AskPrice8` Nullable(Float32), `AskVolume8` Nullable(Float64), `AskPrice9` Nullable(Float32), `AskVolume9` Nullable(Float64), \
        `AskPrice10` Nullable(Float32), `AskVolume10` Nullable(Float64), `BidPrice1` Nullable(Float32), `BidVolume1` Nullable(Float64), \
        `BidPrice2` Nullable(Float32), `BidVolume2` Nullable(Float64), `BidPrice3` Nullable(Float32), `BidVolume3` Nullable(Float64), \
        `BidPrice4` Nullable(Float32), `BidVolume4` Nullable(Float64), `BidPrice5` Nullable(Float32), `BidVolume5` Nullable(Float64), \
        `BidPrice6` Nullable(Float32), `BidVolume6` Nullable(Float64), `BidPrice7` Nullable(Float32), `BidVolume7` Nullable(Float64), \
        `BidPrice8` Nullable(Float32), `BidVolume8` Nullable(Float64), `BidPrice9` Nullable(Float32), `BidVolume9` Nullable(Float64), \
        `BidPrice10` Nullable(Float32), `BidVolume10` Nullable(Float64), `NumOrdersB1` Nullable(UInt32), `NumOrdersB2` Nullable(UInt32), \
        `NumOrdersB3` Nullable(UInt32), `NumOrdersB4` Nullable(UInt32), `NumOrdersB5` Nullable(UInt32), `NumOrdersB6` Nullable(UInt32), \
        `NumOrdersB7` Nullable(UInt32), `NumOrdersB8` Nullable(UInt32), `NumOrdersB9` Nullable(UInt32), `NumOrdersB10` Nullable(UInt32), \
        `NumOrdersS1` Nullable(UInt32), `NumOrdersS2` Nullable(UInt32), `NumOrdersS3` Nullable(UInt32), `NumOrdersS4` Nullable(UInt32), \
        `NumOrdersS5` Nullable(UInt32), `NumOrdersS6` Nullable(UInt32), `NumOrdersS7` Nullable(UInt32), `NumOrdersS8` Nullable(UInt32), \
        `NumOrdersS9` Nullable(UInt32), `NumOrdersS10` Nullable(UInt32), `LocalTime` String, `SeqNo` UInt64) \
        ENGINE = ReplacingMergeTree PARTITION BY toYYYYMMDD(Date) ORDER BY (PrefixSecurityID, UpdateTime, SecurityID) SETTINGS index_granularity = 8192";
    
    
    client[INSIGHT_MARKET_SH]->Select(create, nullptr);
    debug_print("Execute creation on table MarketSH success...");
    return ClickHouseError::ERROR_SUCCESS;
}


template<>
ClickHouseError ClickHouse::OptimizeStorage<MarketSH>() 
{
    std::string optimize = "optimize table " + market_sh_table 
        + " partition '" + GetCurrentDate() + "' final";
    client[INSIGHT_MARKET_SH]->Select(optimize, nullptr);
    debug_print("Execute optimization on table MarketSH success...");
    return ClickHouseError::ERROR_SUCCESS;
}



template<>
ClickHouseError ClickHouse::DumpStorage<MarketSH>(const MarketSH_t ptr) 
{
    return ClickHouseError::ERROR_SUCCESS;
}

