#include "core/click_house/ClickHouse.h"
#include "core/datayes_weave/QueueSH.h"


template<>
ClickHouseError ClickHouse::UpdateStorage<QueueSH>(QueueSH_t ptr) 
{
    static int count = 0;;
    static clickhouse::Block block;
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, Volume);
    DEFINE_QUEUE_COLUMNS_NULLABLE(block, clickhouse::ColumnFloat64, OrderQty, ORDER_QUEUE_SIZE);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnUInt64, NumOrders);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnUInt64, NoOrders);
    DEFINE_COLUMN(block, clickhouse::ColumnUInt64, SeqNo);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat32, Price);
    DEFINE_COLUMN(block, clickhouse::ColumnUInt8, ImageStatus);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnUInt8, NoPriceLevel);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnUInt8, PrcLvlOperator);
    DEFINE_COLUMN(block, clickhouse::ColumnString, UpdateTime);
    DEFINE_COLUMN(block, clickhouse::ColumnString, LocalTime);
    DEFINE_DATE_COLUMN(block, Date);
    DEFINE_FIXED_COLUMN(block, SecurityID, 6);
    DEFINE_FIXED_COLUMN(block, PrefixSecurityID, 1);
    DEFINE_FIXED_COLUMN(block, Side, 1);
    
    if (ptr) {
        
        if (ptr->NoOrders <= 0)
            return ClickHouseError::ERROR_SUCCESS;
        
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, Volume, ptr);
        APPEND_QUEUE_COLUMNS_NULLABLE(clickhouse::ColumnFloat64, OrderQty, ORDER_QUEUE_SIZE, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnUInt64, NumOrders, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnUInt64, NoOrders, ptr);
        APPEND_COLUMN(clickhouse::ColumnUInt64, SeqNo, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat32, Price, ptr);
        APPEND_COLUMN(clickhouse::ColumnUInt8, ImageStatus, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnUInt8, NoPriceLevel, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnUInt8, PrcLvlOperator, ptr);
        APPEND_COLUMN(clickhouse::ColumnString, UpdateTime, ptr);
        APPEND_COLUMN(clickhouse::ColumnString, LocalTime, ptr);
        APPEND_DATE_COLUMN(Date, ptr);
        APPEND_FIXED_COLUMN(SecurityID, 6, ptr);
        APPEND_FIXED_COLUMN(PrefixSecurityID, 1, ptr);
        APPEND_FIXED_COLUMN(Side, 1, ptr);
        
        count++;

        // When the threshold is reached, it is stored in the database
        if (count < QUEUE_SH_BATCH) 
            return ClickHouseError::ERROR_SUCCESS;

    }

    // nullptr sent periodically are signals that are stored in the database
    if (count <= 0)
        return ClickHouseError::ERROR_SUCCESS;

    block.RefreshRowCount();
    debug_print("[%d,%d] row-column will be stored into table: %s", count, block.GetColumnCount(), queue_sh_table.c_str());
    count = 0;

    try {
#ifndef DATA_TEST
        client[INSIGHT_QUEUE_SH]->Insert(queue_sh_table, block);
#endif
        clear_block(block);
    } 
    catch (std::exception& e) {
        warning_print("%s: %s", __FUNCTION__, e.what());
        client[INSIGHT_QUEUE_SH] = ClickHouse::GetInstance()->init_client();
        if (block.GetRowCount() > MULTIPLE * QUEUE_SH_BATCH) {
            clear_block(block);
            error_print("Buffer overflow, discarding data sent to table: %s", queue_sh_table.c_str());
        }
    }

    return ClickHouseError::ERROR_SUCCESS;
}

template<>
ClickHouseError ClickHouse::OptimizeStorage<QueueSH>() 
{
    std::string optimize = "optimize table " + queue_sh_table
        + " partition '" + GetCurrentDate() + "' final";
    client[INSIGHT_QUEUE_SH]->Select(optimize, nullptr);
    debug_print("Execute optimization on table QueueSH success...");
    return ClickHouseError::ERROR_SUCCESS;
}

template<>
ClickHouseError ClickHouse::CreateStorage<QueueSH>() 
{
    std::string create = "CREATE TABLE IF NOT EXISTS " + queue_sh_table + 
    " (`Date` Date, `PrefixSecurityID` LowCardinality(FixedString(1)), `UpdateTime` String, `SecurityID` LowCardinality(FixedString(6)), \
    `ImageStatus` UInt8, `Side` LowCardinality(FixedString(1)), `NoPriceLevel` Nullable(UInt8), `PrcLvlOperator` Nullable(UInt8), \
    `Price` Nullable(Float32), `Volume` Nullable(Float64), `NumOrders` Nullable(UInt64), `NoOrders` Nullable(UInt64), \
    `OrderQty1` Nullable(Float64), `OrderQty2` Nullable(Float64), `OrderQty3` Nullable(Float64), `OrderQty4` Nullable(Float64), \
    `OrderQty5` Nullable(Float64), `OrderQty6` Nullable(Float64), `OrderQty7` Nullable(Float64), `OrderQty8` Nullable(Float64), \
    `OrderQty9` Nullable(Float64), `OrderQty10` Nullable(Float64), `OrderQty11` Nullable(Float64), `OrderQty12` Nullable(Float64), \
    `OrderQty13` Nullable(Float64), `OrderQty14` Nullable(Float64), `OrderQty15` Nullable(Float64), `OrderQty16` Nullable(Float64), \
    `OrderQty17` Nullable(Float64), `OrderQty18` Nullable(Float64), `OrderQty19` Nullable(Float64), `OrderQty20` Nullable(Float64), \
    `OrderQty21` Nullable(Float64), `OrderQty22` Nullable(Float64), `OrderQty23` Nullable(Float64), `OrderQty24` Nullable(Float64), \
    `OrderQty25` Nullable(Float64), `OrderQty26` Nullable(Float64), `OrderQty27` Nullable(Float64), `OrderQty28` Nullable(Float64), \
    `OrderQty29` Nullable(Float64), `OrderQty30` Nullable(Float64), `OrderQty31` Nullable(Float64), `OrderQty32` Nullable(Float64), \
    `OrderQty33` Nullable(Float64), `OrderQty34` Nullable(Float64), `OrderQty35` Nullable(Float64), `OrderQty36` Nullable(Float64), \
    `OrderQty37` Nullable(Float64), `OrderQty38` Nullable(Float64), `OrderQty39` Nullable(Float64), `OrderQty40` Nullable(Float64), \
    `OrderQty41` Nullable(Float64), `OrderQty42` Nullable(Float64), `OrderQty43` Nullable(Float64), `OrderQty44` Nullable(Float64), \
    `OrderQty45` Nullable(Float64), `OrderQty46` Nullable(Float64), `OrderQty47` Nullable(Float64), `OrderQty48` Nullable(Float64), \
    `OrderQty49` Nullable(Float64), `OrderQty50` Nullable(Float64), `LocalTime` String, `SeqNo` UInt64) \
    ENGINE = ReplacingMergeTree PARTITION BY toYYYYMMDD(Date) ORDER BY (Side, PrefixSecurityID, UpdateTime, SecurityID) SETTINGS index_granularity = 8192";

    client[INSIGHT_QUEUE_SH]->Select(create, nullptr);
    debug_print("Execute creation on table QueueSH success...");
    return ClickHouseError::ERROR_SUCCESS;
}


template<>
ClickHouseError ClickHouse::DumpStorage<QueueSH>(const QueueSH_t ptr) 
{
    return ClickHouseError::ERROR_SUCCESS;
}

