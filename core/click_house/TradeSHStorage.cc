#include "core/click_house/ClickHouse.h"
#include "core/datayes_weave/TradeSH.h"


template<>
ClickHouseError ClickHouse::UpdateStorage<TradeSH>(TradeSH_t ptr) 
{
    static int count = 0;;
    static clickhouse::Block block;
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, TradVolume);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat64, TradeMoney);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt64, TradeBuyNo);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnInt64, TradeSellNo);
    DEFINE_COLUMN(block, clickhouse::ColumnInt64, TradeIndex);
    DEFINE_COLUMN(block, clickhouse::ColumnInt64, TradeChan);
    DEFINE_COLUMN(block, clickhouse::ColumnUInt64, SeqNo);
    DEFINE_COLUMN_NULLABLE(block, clickhouse::ColumnFloat32, TradPrice);
    DEFINE_COLUMN(block, clickhouse::ColumnUInt8, DataStatus);
    DEFINE_COLUMN(block, clickhouse::ColumnString, TradTime);
    DEFINE_COLUMN(block, clickhouse::ColumnString, LocalTime);
    DEFINE_DATE_COLUMN(block, Date);
    DEFINE_FIXED_COLUMN(block, SecurityID, 6);
    DEFINE_FIXED_COLUMN(block, PrefixSecurityID, 1);
    DEFINE_FIXED_COLUMN(block, TradeBSFlag, 1);
    
    if (ptr) {
        
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, TradVolume, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat64, TradeMoney, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt64, TradeBuyNo, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnInt64, TradeSellNo, ptr);
        APPEND_COLUMN(clickhouse::ColumnInt64, TradeIndex, ptr);
        APPEND_COLUMN(clickhouse::ColumnInt64, TradeChan, ptr);
        APPEND_COLUMN(clickhouse::ColumnUInt64, SeqNo, ptr);
        APPEND_COLUMN_NULLABLE(clickhouse::ColumnFloat32, TradPrice, ptr);
        APPEND_COLUMN(clickhouse::ColumnUInt8, DataStatus, ptr);
        APPEND_COLUMN(clickhouse::ColumnString, TradTime, ptr);
        APPEND_COLUMN(clickhouse::ColumnString, LocalTime, ptr);
        APPEND_DATE_COLUMN(Date, ptr);
        APPEND_FIXED_COLUMN(SecurityID, 6, ptr);
        APPEND_FIXED_COLUMN(PrefixSecurityID, 1, ptr);
        APPEND_FIXED_COLUMN(TradeBSFlag, 1, ptr);
        count++;

        // When the threshold is reached, it is stored in the database
        if (count < TRADE_SH_BATCH) 
            return ClickHouseError::ERROR_SUCCESS;

    }
    
    // nullptr sent periodically are signals that are stored in the database
    if (count <= 0)
        return ClickHouseError::ERROR_SUCCESS;

    block.RefreshRowCount();
    debug_print("[%d,%d] row-column will be stored into table: %s", count, block.GetColumnCount(), trade_sh_table.c_str());
    count = 0;

    try {
#ifndef DATA_TEST
        client[INSIGHT_TRADE_SH]->Insert(trade_sh_table, block);
#endif
        clear_block(block);
    } 
    catch (std::exception& e) {
        warning_print("%s: %s", __FUNCTION__, e.what());
        client[INSIGHT_TRADE_SH] = ClickHouse::GetInstance()->init_client();
        if (block.GetRowCount() > MULTIPLE * TRADE_SH_BATCH) {
            clear_block(block);
            error_print("Buffer overflow, discarding data sent to table: %s", trade_sh_table.c_str());
        }
    }

    return ClickHouseError::ERROR_SUCCESS;
}

template<>
ClickHouseError ClickHouse::OptimizeStorage<TradeSH>() 
{
    std::string optimize = "optimize table " + trade_sh_table
        + " partition '" + GetCurrentDate() + "' final";
    client[INSIGHT_TRADE_SH]->Select(optimize, nullptr);
    debug_print("Execute optimization on table TradeSH success...");
    return ClickHouseError::ERROR_SUCCESS;
}

template<>
ClickHouseError ClickHouse::CreateStorage<TradeSH>() 
{
    std::string create = "CREATE TABLE IF NOT EXISTS " + trade_sh_table + 
    " (`Date` Date, `PrefixSecurityID` LowCardinality(FixedString(1)), `DataStatus` UInt8, `TradeIndex` Int64, \
    `TradeChan` Int64, `SecurityID` LowCardinality(FixedString(6)), `TradTime` String, `TradPrice` Nullable(Float32), \
    `TradVolume` Nullable(Float64), `TradeMoney` Nullable(Float64), `TradeBuyNo` Nullable(Int64), `TradeSellNo` Nullable(Int64), \
    `TradeBSFlag` LowCardinality(FixedString(1)), `LocalTime` String, `SeqNo` UInt64) \
    ENGINE = ReplacingMergeTree PARTITION BY toYYYYMMDD(Date) ORDER BY (PrefixSecurityID, TradTime, SecurityID, TradeIndex) SETTINGS index_granularity = 8192";

    client[INSIGHT_TRADE_SH]->Select(create, nullptr);
    debug_print("Execute creation on table TradeSH success...");
    return ClickHouseError::ERROR_SUCCESS;
}


template<>
ClickHouseError ClickHouse::DumpStorage<TradeSH>(const TradeSH_t ptr) 
{
    return ClickHouseError::ERROR_SUCCESS;
}

