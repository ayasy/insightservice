#include "core/click_house/ClickHouse.h"
#include "core/common_tool/SimpleIni.h"
#include "core/datayes_weave/MarketSH.h"
#include "core/datayes_weave/MarketSZ.h"
#include "core/datayes_weave/OrderSZ.h"
#include "core/datayes_weave/TradeSH.h"
#include "core/datayes_weave/TradeSZ.h"
#include "core/datayes_weave/QueueSH.h"
#include "core/datayes_weave/QueueSZ.h"
#include "core/datayes_weave/OrderSH.h"

void ClickHouse::init()
{
    load_config();
    for (int i = 0; i < CLICK_HOUSE_PIPE; ++i) 
        client[i] = init_client();
}

ClickHouse* ClickHouse::GetInstance() 
{
    static ClickHouse instance;
    return &instance;
}

std::unique_ptr<clickhouse::Client> ClickHouse::init_client() {
    return std::make_unique<clickhouse::Client>(
                        clickhouse::ClientOptions().
                        SetHost(ip).SetPort(port).
                        SetUser(username).SetPassword(password).
                        SetSendRetries(3).TcpKeepAlive(true));
}

std::string ClickHouse::get_table_sh_name(const std::string& name) {
    return DATABASE_PREFIX + name + "." + TABLE_SH_PREFIX + std::string(month);
}

std::string ClickHouse::get_table_sz_name(const std::string& name) {
    return DATABASE_PREFIX + name + "." + TABLE_SZ_PREFIX + std::string(month);
}

void ClickHouse::load_config() {
    CSimpleIniA ini;
	SI_Error rc = ini.LoadFile("./config/config.ini");
    if (rc < 0) {
        throw("config file not exists! Use default config instead ...");
    };
    
    const char* value;
    value = ini.GetValue("CLICKHOUSE", "username", "");
    username = std::string(value);
    value = ini.GetValue("CLICKHOUSE", "password", "");
    password = std::string(value);
    value = ini.GetValue("CLICKHOUSE", "ip", "");
    ip = std::string(value);
    port = ini.GetLongValue("CLICKHOUSE", "port");
    COEFFICIENT = ini.GetLongValue("COUNTER", "COEFFICIENT");
    MARKET_SH_BATCH = ini.GetLongValue("COUNTER", "MARKET_SH_BATCH") * COEFFICIENT;
    MARKET_SZ_BATCH = ini.GetLongValue("COUNTER", "MARKET_SZ_BATCH") * COEFFICIENT;
    ORDER_SZ_BATCH = ini.GetLongValue("COUNTER", "ORDER_SZ_BATCH") * COEFFICIENT;
    TRADE_SH_BATCH = ini.GetLongValue("COUNTER", "TRADE_SH_BATCH") * COEFFICIENT;
    TRADE_SZ_BATCH = ini.GetLongValue("COUNTER", "TRADE_SZ_BATCH") * COEFFICIENT;
    QUEUE_SH_BATCH = ini.GetLongValue("COUNTER", "QUEUE_SH_BATCH") * COEFFICIENT;
    QUEUE_SZ_BATCH = ini.GetLongValue("COUNTER", "QUEUE_SZ_BATCH") * COEFFICIENT;
    ORDER_SH_BATCH = ini.GetLongValue("COUNTER", "ORDER_SH_BATCH") * COEFFICIENT;
    value = ini.GetValue("PREFIX", "DATABASE_PREFIX", "");
    DATABASE_PREFIX = std::string(value);
    value = ini.GetValue("PREFIX", "TABLE_SH_PREFIX", "");
    TABLE_SH_PREFIX = std::string(value);
    value = ini.GetValue("PREFIX", "TABLE_SZ_PREFIX", "");
    TABLE_SZ_PREFIX = std::string(value);
 }

std::shared_ptr<CK_LOW_FIXED_STRING> ClickHouse::init_fixed_column(const std::string& name, std::size_t length, clickhouse::Block& block) {
    std::shared_ptr<CK_LOW_FIXED_STRING> ptr = std::make_shared<CK_LOW_FIXED_STRING>(length);
    block.AppendColumn(name, ptr);
    return ptr;
}

std::shared_ptr<clickhouse::ColumnDate> ClickHouse::init_date_column(const std::string& name, clickhouse::Block& block) {
    std::shared_ptr<clickhouse::ColumnDate> ptr = std::make_shared<clickhouse::ColumnDate>();   
    block.AppendColumn(name, ptr);
    return ptr;
}


void ClickHouse::clear_block(clickhouse::Block& block) {
    for (uint64_t i = 0; i < block.GetColumnCount(); ++i) {
        if (block[i]->Type()->GetCode() == clickhouse::Type::Code::Nullable) {
            block[i]->As<clickhouse::ColumnNullable>()->Nested()->Clear();
            block[i]->As<clickhouse::ColumnNullable>()->Nulls()->Clear();
        } else {
            block[i]->Clear();
        }
    }
}

void ClickHouse::OptimizeToday() {
    SetCurrentMonth(GetCurrentDate());
    debug_print("begin optimizing MarketSH...");
    OptimizeStorage<MarketSH>();
    debug_print("begin optimizing MarketSZ...");
    OptimizeStorage<MarketSZ>();
    debug_print("begin optimizing OrderSZ...");
    OptimizeStorage<OrderSZ>();
    debug_print("begin optimizing TradeSH...");
    OptimizeStorage<TradeSH>();
    debug_print("begin optimizing TradeSZ...");
    OptimizeStorage<TradeSZ>();
    debug_print("begin optimizing QueueSH...");
    OptimizeStorage<QueueSH>();
    debug_print("begin optimizing QueueSZ...");
    OptimizeStorage<QueueSZ>();
    debug_print("begin optimizing OrderSH...");
    OptimizeStorage<OrderSH>();
}

void ClickHouse::CreateCurrentMonth() {
    CreateStorage<MarketSH>();
    CreateStorage<MarketSZ>();
    CreateStorage<OrderSZ>();
    CreateStorage<TradeSH>();
    CreateStorage<TradeSZ>();
    CreateStorage<QueueSH>();
    CreateStorage<QueueSZ>();
    CreateStorage<OrderSH>();
    debug_print("storage creation finished...");
}


void ClickHouse::SetCurrentMonth(const std::string& date) {
    if (date.size() < 6)
        return;
    if (date.substr(0, 6) == std::string(month))
        return;

    snprintf(month, 7, "%s", date.c_str());
    market_sh_table = ClickHouse::GetInstance()->get_table_sh_name("MarketData");
    market_sz_table = ClickHouse::GetInstance()->get_table_sz_name("MarketData");
    order_sz_table = ClickHouse::GetInstance()->get_table_sz_name("Order");
    trade_sh_table = ClickHouse::GetInstance()->get_table_sh_name("Trade");
    trade_sz_table = ClickHouse::GetInstance()->get_table_sz_name("Trade");
    queue_sh_table = ClickHouse::GetInstance()->get_table_sh_name("OrderQueue");
    queue_sz_table = ClickHouse::GetInstance()->get_table_sz_name("OrderQueue");
    order_sh_table = ClickHouse::GetInstance()->get_table_sh_name("Order");
}
