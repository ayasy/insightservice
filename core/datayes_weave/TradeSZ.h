#pragma once

#include "core/datayes_weave/BaseInfo.h"

class TradeSZ : public BaseInfo 
{
public:
    TradeSZ() {}
    ~TradeSZ() {}
    
public:
    double LastPx;
    int64_t ApplSeqNum;
    int64_t BidApplSeqNum;
    int64_t OfferApplSeqNum;
    int64_t LastQty;
    int64_t TradeIndex;
    uint64_t SeqNo;
    int32_t ChannelNo;
    int32_t ExecType;
    char TransactTime[13];
    char LocalTime[13];
    char Date[11];
    char SecurityID[7];
    char MDStreamID[4];
    char SecurityIDSource[4];
    char PrefixSecurityID[3];
};


typedef TradeSZ* TradeSZ_t;
