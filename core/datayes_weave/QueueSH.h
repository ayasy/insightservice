#pragma once

#include "core/datayes_weave/BaseInfo.h"

class QueueSH : public BaseInfo 
{
public:
    QueueSH() {}
    ~QueueSH() {}
    
public:
    double Volume;
    double OrderQty[50];
    uint64_t NumOrders;
    uint64_t NoOrders;
    uint64_t SeqNo;
    float Price;
    uint8_t ImageStatus;
    uint8_t NoPriceLevel;
    uint8_t PrcLvlOperator;
    char UpdateTime[13];
    char LocalTime[13];
    char Date[11];
    char SecurityID[7];
    char PrefixSecurityID[2];
    char Side[2];

};

typedef QueueSH* QueueSH_t;
