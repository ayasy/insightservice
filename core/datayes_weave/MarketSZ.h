#pragma once

#include "core/datayes_weave/BaseInfo.h"

class MarketSZ : public BaseInfo 
{
public:
    MarketSZ() {}
    ~MarketSZ() {}
    
public:

    double PreCloPrice;
    double Turnover;
    double LastPrice;
    double OpenPrice;
    double HighPrice;
    double LowPrice;
    double DifPrice1;
    double DifPrice2;
    double PE1;
    double PE2;
#ifdef MARKET_FULL
    double PreCloseIOPV;
    double IOPV;
    double OptPremiumRatio;
#endif
    double WeightedAvgBidPx;
    double WeightedAvgOfferPx;
    double HighLimitPrice;
    double LowLimitPrice;
    double AskPrice[10];
    double BidPrice[10];
    int64_t TurnNum;
    int64_t Volume;
    int64_t TotalBidQty;
    int64_t TotalOfferQty;
    int64_t OpenInt;
    int64_t AskVolume[10];
    int64_t BidVolume[10];
    uint64_t SeqNo;
    uint32_t NumOrdersB[10];
    uint32_t NumOrdersS[10];
    char UpdateTime[13];
    char LocalTime[13];
    char Date[11];
    char SecurityID[7];
    char SecurityIDSource[4];
    char MDStreamID[4];
    char PrefixSecurityID[3];
    char TradingPhaseCode[3];

};


typedef MarketSZ* MarketSZ_t;
