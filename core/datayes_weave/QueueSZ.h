#pragma once

#include "core/datayes_weave/BaseInfo.h"

class QueueSZ : public BaseInfo 
{
public:
    QueueSZ() {}
    ~QueueSZ() {}
    
public:
    double Price;
    int64_t Volume;
    uint64_t NumOrders;
    uint64_t NoOrders;
    uint64_t SeqNo;
    uint64_t OrderQty[50];
    uint8_t ImageStatus;
    uint8_t NoPriceLevel;
    uint8_t PrcLvlOperator;
    char DataTimeStamp[13];
    char LocalTime[13];
    char Date[11];
    char SecurityID[7];
    char PrefixSecurityID[3];
    char Side[2];

};

typedef QueueSZ* QueueSZ_t;
