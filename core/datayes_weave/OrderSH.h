#pragma once

#include "core/datayes_weave/BaseInfo.h"

class OrderSH : public BaseInfo
{
public:
    OrderSH() {}
    ~OrderSH() {}
    
public:
    double Price;
    int64_t ApplSeqNum;
    int64_t OrderQty;
    int64_t OrderIndex;
    int64_t OrderNo;
    uint64_t SeqNo;
    int32_t ChannelNo;
    int32_t Side;
    int32_t OrdType;
    char TransactTime[13];
    char LocalTime[13];
    char Date[11];
    char SecurityID[7];
    char MDStreamID[4];
    char SecurityIDSource[4];
    char PrefixSecurityID[2];

};

typedef OrderSH* OrderSH_t;
