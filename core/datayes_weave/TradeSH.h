#pragma once

#include "core/datayes_weave/BaseInfo.h"

class TradeSH : public BaseInfo
{
public:
    TradeSH() {}
    ~TradeSH() {}
    
public:
    double TradVolume;
    double TradeMoney;
    int64_t TradeBuyNo;
    int64_t TradeSellNo;
    int64_t TradeIndex;
    int64_t TradeChan;
    uint64_t SeqNo;
    float TradPrice;
    uint8_t DataStatus;
    char TradTime[13];
    char LocalTime[13];
    char Date[11];
    char SecurityID[7];
    char PrefixSecurityID[2];
    char TradeBSFlag[2];
};

typedef TradeSH* TradeSH_t;
