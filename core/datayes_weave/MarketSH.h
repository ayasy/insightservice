#pragma once

#include "core/datayes_weave/BaseInfo.h"

class MarketSH : public BaseInfo 
{
public:
    MarketSH() {}
    ~MarketSH() {}
    
    
public:
    // reduce memory usage according to memory alignment rules
    double TradVolume;
    double Turnover;
    double TotalBidVol;
    double TotalAskVol;
#ifdef MARKET_FULL
    double EtfBuyVolume;
    double EtfBuyMoney;
    double EtfSellVolume;
    double ETFSellMoney;
    double TotWarExNum;
    double WarLowerPri;
    double WarUpperPri;
#endif
    double WiDBuyVol;
    double WiDBuyMon;
    double WiDSellVol;
    double WiDSellMon;
    double AskVolume[10];
    double BidVolume[10];
    int64_t MaxBidDur;
    int64_t MaxSellDur;
    uint64_t SeqNo;
    float PreCloPrice;
    float OpenPrice;
    float HighPrice;
    float LowPrice;
    float LastPrice;
    float ClosePrice;
    float WAvgBidPri;
    float WAvgAskPri;
    float AskPrice[10];
    float BidPrice[10];
#ifdef MARKET_FULL
    float AltWAvgBidPri;
    float AltWAvgAskPri;
    float YieldToMatu;
    float IOPV;
    uint32_t EtfBuyNumber;
    uint32_t EtfSellNumber;
#endif
    uint32_t WiDBuyNum;
    uint32_t WiDSellNum;
    uint32_t TotBidNum;
    uint32_t TotSellNum;
    uint32_t BidNum;
    uint32_t SellNum;
    uint32_t TradNumber;
    uint32_t NumOrdersB[10];
    uint32_t NumOrdersS[10];
    uint8_t ImageStatus;
    char UpdateTime[13];
    char LocalTime[13];
    char Date[11];
    char InstruStatus[8];
    char SecurityID[7];
    char PrefixSecurityID[2];

};

typedef MarketSH* MarketSH_t;
