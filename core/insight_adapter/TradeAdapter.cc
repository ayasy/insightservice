#include "core/insight_adapter/AdapterRegistry.h"
#include "core/datayes_weave/TradeSH.h"
#include "core/datayes_weave/TradeSZ.h"
#include "core/common_tool/CommonTool.h"

template<>
void MarketAdapter::MarketParse<TradeSH>(const InsightMarket& market, TradeSH_t ptr) 
{
    auto trade = market.mdtransaction();
    ptr->TradVolume = static_cast<double>(trade.tradeqty());
    ptr->TradeMoney = trade.trademoney() * TEN_THOUSANDTH;
    ptr->TradeBuyNo = trade.tradebuyno();
    ptr->TradeSellNo = trade.tradesellno();
    ptr->TradeIndex = trade.tradeindex();
    ptr->TradeChan = trade.channelno();
    ptr->TradPrice = trade.tradeprice() * TEN_THOUSANDTH;
    snprintf(ptr->TradTime, 13, "%s", TransferTime(trade.mdtime()).c_str());
    snprintf(ptr->LocalTime, 13, "%s", TransferTime(!trade.exchangetime() ? trade.mdtime() : trade.exchangetime()).c_str());
    snprintf(ptr->Date, 11, "%s", TransferDate(trade.mddate()).c_str());
    snprintf(ptr->SecurityID, 7, "%s", trade.htscsecurityid().c_str());
    snprintf(ptr->PrefixSecurityID, 2, "%c", ptr->SecurityID[0]);
    snprintf(ptr->TradeBSFlag, 2, "%c", TransferOrderFlagSH(trade.tradebsflag()));
}



template<>
void MarketAdapter::MarketParse<TradeSZ>(const InsightMarket& market, TradeSZ_t ptr) 
{
    auto trade = market.mdtransaction();
    ptr->LastPx = trade.tradeprice() * TEN_THOUSANDTH;
    ptr->ApplSeqNum = trade.applseqnum();
    ptr->BidApplSeqNum = trade.tradebuyno();
    ptr->OfferApplSeqNum = trade.tradesellno();
    ptr->TradeIndex = trade.tradeindex();
    ptr->LastQty = trade.tradeqty();
    ptr->ChannelNo = trade.channelno();
    ptr->ExecType = trade.tradetype() == 0 ? 70 : 52;
    snprintf(ptr->TransactTime, 13, "%s", TransferTime(trade.mdtime()).c_str());
    snprintf(ptr->LocalTime, 13, "%s", TransferTime(!trade.exchangetime() ? trade.mdtime() : trade.exchangetime()).c_str());
    snprintf(ptr->Date, 11, "%s", TransferDate(trade.mddate()).c_str());
    snprintf(ptr->SecurityID, 7, "%s", trade.htscsecurityid().c_str());
    snprintf(ptr->MDStreamID, 4, "%s", TransferMarketTypeSZ(trade.securitytype()).c_str());
    snprintf(ptr->SecurityIDSource, 4, "102");
    snprintf(ptr->PrefixSecurityID, 3, "%c%c", ptr->SecurityID[0], ptr->SecurityID[1]);
}


