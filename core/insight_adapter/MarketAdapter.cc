#include "core/insight_adapter/AdapterRegistry.h"
#include "core/datayes_weave/MarketSH.h"
#include "core/datayes_weave/MarketSZ.h"
#include "core/common_tool/CommonTool.h"

#define MARKET_SPREAD(TARGET, SOURCE, SIZE, FUNC) \
    for (int i = 0; i < SIZE; ++i) { \
        if (i < SOURCE.size())  \
            TARGET[i] = FUNC(SOURCE[i]); \
        else    \
            TARGET[i] = PLACE_HOLDER;\
    }

template<>
void MarketAdapter::MarketParse<MarketSH>(const InsightMarket& market, MarketSH_t ptr) 
{
    auto stock = market.mdstock();
    ptr->TradVolume = static_cast<double>(stock.totalvolumetrade());
    ptr->Turnover = static_cast<double>(stock.totalvaluetrade());
    ptr->TotalBidVol = stock.totalbuyqty();
    ptr->TotalAskVol = stock.totalsellqty();
    ptr->WiDBuyVol = static_cast<double>(stock.withdrawbuyamount());
    ptr->WiDBuyMon = static_cast<double>(stock.withdrawbuymoney());
    ptr->WiDSellVol = static_cast<double>(stock.withdrawsellamount());
    ptr->WiDSellMon = static_cast<double>(stock.withdrawsellmoney());
    MARKET_SPREAD(ptr->BidVolume, stock.buyorderqtyqueue(), 10, static_cast<double>);
    MARKET_SPREAD(ptr->AskVolume, stock.sellorderqtyqueue(), 10, static_cast<double>);
    ptr->MaxBidDur = stock.buytrademaxduration();
    ptr->MaxSellDur = stock.selltrademaxduration();
    ptr->PreCloPrice = stock.preclosepx() * TEN_THOUSANDTH;
    ptr->OpenPrice = stock.openpx() * TEN_THOUSANDTH;
    ptr->HighPrice = stock.highpx() * TEN_THOUSANDTH;
    ptr->LowPrice = stock.lowpx() * TEN_THOUSANDTH;
    ptr->LastPrice = stock.lastpx() * TEN_THOUSANDTH;
    ptr->ClosePrice = stock.closepx() * TEN_THOUSANDTH;
    ptr->WAvgBidPri = stock.weightedavgbuypx() * TEN_THOUSANDTH;
    ptr->WAvgAskPri = stock.weightedavgsellpx() * TEN_THOUSANDTH;
    MARKET_SPREAD(ptr->BidPrice, stock.buypricequeue(), 10, TEN_THOUSANDTH*);
    MARKET_SPREAD(ptr->AskPrice, stock.sellpricequeue(), 10, TEN_THOUSANDTH*);
    ptr->WiDBuyNum = stock.withdrawbuynumber();
    ptr->WiDSellNum = stock.withdrawsellnumber();
    ptr->TotBidNum = stock.totalbuynumber();
    ptr->TotSellNum = stock.totalsellnumber();
    ptr->BidNum = stock.numbuyorders();
    ptr->SellNum = stock.numsellorders();
    ptr->TradNumber = stock.numtrades();
    MARKET_SPREAD(ptr->NumOrdersB, stock.buynumordersqueue(), 10, static_cast<uint32_t>);
    MARKET_SPREAD(ptr->NumOrdersS, stock.sellnumordersqueue(), 10, static_cast<uint32_t>);
    snprintf(ptr->UpdateTime, 13, "%s", TransferTime(stock.mdtime()).c_str());
    snprintf(ptr->LocalTime, 13, "%s", TransferTime(!stock.exchangetime() ? stock.mdtime() : stock.exchangetime()).c_str());
    snprintf(ptr->Date, 11, "%s", TransferDate(stock.mddate()).c_str());
    snprintf(ptr->InstruStatus, 8, "%s", TransferStatusSH(stock.tradingphasecode()).c_str());
    snprintf(ptr->SecurityID, 7, "%s", stock.htscsecurityid().c_str());
    snprintf(ptr->PrefixSecurityID, 2, "%c", ptr->SecurityID[0]);
}



template<>
void MarketAdapter::MarketParse<MarketSZ>(const InsightMarket& market, MarketSZ_t ptr) 
{
    auto stock = market.mdstock();
    ptr->PreCloPrice = stock.preclosepx() * TEN_THOUSANDTH;
    ptr->Turnover = static_cast<double>(stock.totalvaluetrade());
    ptr->LastPrice = stock.lastpx() * TEN_THOUSANDTH;
    ptr->OpenPrice = stock.openpx() * TEN_THOUSANDTH;
    ptr->HighPrice = stock.highpx() * TEN_THOUSANDTH;
    ptr->LowPrice = stock.lowpx() * TEN_THOUSANDTH;
    ptr->DifPrice1 = stock.diffpx1() * TEN_THOUSANDTH;
    ptr->DifPrice2 = stock.diffpx2() * TEN_THOUSANDTH;
    ptr->WeightedAvgBidPx = stock.weightedavgbuypx() * TEN_THOUSANDTH;
    ptr->WeightedAvgOfferPx = stock.weightedavgsellpx() * TEN_THOUSANDTH;
    ptr->HighLimitPrice = stock.maxpx() * TEN_THOUSANDTH;
    ptr->LowLimitPrice = stock.minpx() * TEN_THOUSANDTH;
    MARKET_SPREAD(ptr->BidPrice, stock.buypricequeue(), 10, TEN_THOUSANDTH*);
    MARKET_SPREAD(ptr->AskPrice, stock.sellpricequeue(), 10, TEN_THOUSANDTH*);
    ptr->TurnNum = stock.numtrades();
    ptr->Volume = stock.totalvolumetrade();
    ptr->TotalBidQty = stock.totalbuyqty();
    ptr->TotalOfferQty = stock.totalsellqty();
    MARKET_SPREAD(ptr->BidVolume, stock.buyorderqtyqueue(), 10, 1*);
    MARKET_SPREAD(ptr->AskVolume, stock.sellorderqtyqueue(), 10, 1*);
    MARKET_SPREAD(ptr->NumOrdersB, stock.buynumordersqueue(), 10, static_cast<uint32_t>);
    MARKET_SPREAD(ptr->NumOrdersS, stock.sellnumordersqueue(), 10, static_cast<uint32_t>);
    snprintf(ptr->UpdateTime, 13, "%s", TransferTime(stock.mdtime()).c_str());
    snprintf(ptr->LocalTime, 13, "%s", TransferTime(!stock.exchangetime() ? stock.mdtime() : stock.exchangetime()).c_str());
    snprintf(ptr->Date, 11, "%s", TransferDate(stock.mddate()).c_str());
    snprintf(ptr->SecurityID, 7, "%s", stock.htscsecurityid().c_str());
    snprintf(ptr->SecurityIDSource, 4, "102");
    snprintf(ptr->MDStreamID, 4, "%s", TransferSecurityTypeSZ(stock.securitytype()).c_str());
    snprintf(ptr->PrefixSecurityID, 3, "%c%c", ptr->SecurityID[0], ptr->SecurityID[1]);
    snprintf(ptr->TradingPhaseCode, 3, "%s", TransferStatusSZ(stock.tradingphasecode()).c_str());
}

#undef MARKET_SPREAD

