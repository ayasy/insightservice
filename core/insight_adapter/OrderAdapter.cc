#include "core/insight_adapter/AdapterRegistry.h"
#include "core/datayes_weave/OrderSZ.h"
#include "core/datayes_weave/OrderSH.h"
#include "core/common_tool/CommonTool.h"

template<>
void MarketAdapter::MarketParse<OrderSZ>(const InsightMarket& market, OrderSZ_t ptr) 
{
    auto order = market.mdorder();
    ptr->Price = order.orderprice() * TEN_THOUSANDTH;
    ptr->ApplSeqNum = order.applseqnum();
    ptr->OrderQty = order.orderqty();
    ptr->ChannelNo = order.channelno();
    ptr->OrderIndex = order.orderindex();
    ptr->Side = TransferOrderSideSZ(order.orderbsflag());
    ptr->OrdType = TransferOrderTypeSZ(order.ordertype());
    snprintf(ptr->TransactTime, 13, "%s", TransferTime(order.mdtime()).c_str());
    snprintf(ptr->LocalTime, 13, "%s", TransferTime(!order.exchangetime() ? order.mdtime() : order.exchangetime()).c_str());
    snprintf(ptr->Date, 11, "%s", TransferDate(order.mddate()).c_str());
    snprintf(ptr->SecurityID, 7, "%s", order.htscsecurityid().c_str());
    snprintf(ptr->MDStreamID, 4, "%s", TransferMarketTypeSZ(order.securitytype()).c_str());
    snprintf(ptr->SecurityIDSource, 4, "102");
    snprintf(ptr->PrefixSecurityID, 3, "%c%c", ptr->SecurityID[0], ptr->SecurityID[1]);

}


template<>
void MarketAdapter::MarketParse<OrderSH>(const InsightMarket& market, OrderSH_t ptr) 
{
    auto order = market.mdorder();
    ptr->Price = order.orderprice() * TEN_THOUSANDTH;
    ptr->ApplSeqNum = order.applseqnum();
    ptr->OrderQty = order.orderqty();
    ptr->OrderIndex = order.orderindex();
    ptr->OrderNo = order.orderno();
    ptr->ChannelNo = order.channelno();
    ptr->Side = TransferOrderSideSZ(order.orderbsflag());
    ptr->OrdType = TransferOrderTypeSZ(order.ordertype());
    snprintf(ptr->TransactTime, 13, "%s", TransferTime(order.mdtime()).c_str());
    snprintf(ptr->LocalTime, 13, "%s", TransferTime(!order.exchangetime() ? order.mdtime() : order.exchangetime()).c_str());
    snprintf(ptr->Date, 11, "%s", TransferDate(order.mddate()).c_str());
    snprintf(ptr->SecurityID, 7, "%s", order.htscsecurityid().c_str());
    snprintf(ptr->MDStreamID, 4, "%s", TransferMarketTypeSZ(order.securitytype()).c_str());
    snprintf(ptr->SecurityIDSource, 4, "101");
    snprintf(ptr->PrefixSecurityID, 2, "%c", ptr->SecurityID[0]);

}

