#include "core/insight_adapter/AdapterRegistry.h"
#include "core/datayes_weave/QueueSH.h"
#include "core/datayes_weave/QueueSZ.h"
#include "core/common_tool/CommonTool.h"

#define MARKET_SPREAD(TARGET, SOURCE, SIZE, FUNC) \
    for (int i = 0; i < SIZE; ++i) { \
        if (i < SOURCE.size())  \
            TARGET[i] = FUNC(SOURCE[i]); \
        else    \
            TARGET[i] = PLACE_HOLDER;\
    }


template<>
void MarketAdapter::MarketParse<QueueSH*>(const InsightMarket& market, QueueSH_t* ptr) 
{
    auto queue = market.mdstock();

    QueueSH_t buy_ptr = ptr[0];
    MARKET_SPREAD(buy_ptr->OrderQty, queue.buyorderqueue(), 50, 1*);
    buy_ptr->NumOrders = queue.buynumordersqueue().size() ? queue.buynumordersqueue()[0] : 0;
    buy_ptr->NoOrders = queue.buyorderqueue().size();
    buy_ptr->Volume = buy_ptr->NoOrders >= 1 ? queue.buyorderqtyqueue()[0]: 0.0;
    buy_ptr->Price = buy_ptr->NoOrders >= 1 ? queue.buypricequeue()[0] * TEN_THOUSANDTH : 0.0;
    buy_ptr->NoPriceLevel = 1;
    snprintf(buy_ptr->UpdateTime, 13, "%s", TransferTime(queue.mdtime()).c_str());
    snprintf(buy_ptr->LocalTime, 13, "%s", TransferTime(!queue.exchangetime() ? queue.mdtime() : queue.exchangetime()).c_str());
    snprintf(buy_ptr->Date, 11, "%s", TransferDate(queue.mddate()).c_str());
    snprintf(buy_ptr->SecurityID, 7, "%s", queue.htscsecurityid().c_str());
    snprintf(buy_ptr->PrefixSecurityID, 2, "%c", buy_ptr->SecurityID[0]);
    snprintf(buy_ptr->Side, 2, "B");
    
    QueueSH_t sell_ptr = ptr[1];
    MARKET_SPREAD(sell_ptr->OrderQty, queue.sellorderqueue(), 50, 1*);
    sell_ptr->NumOrders = queue.sellnumordersqueue().size() ? queue.sellnumordersqueue()[0] : 0;
    sell_ptr->NoOrders = queue.sellorderqueue().size();
    sell_ptr->Volume = sell_ptr->NoOrders >= 1 ? queue.sellorderqtyqueue()[0]: 0.0;
    sell_ptr->Price = sell_ptr->NoOrders >= 1 ? queue.sellpricequeue()[0] * TEN_THOUSANDTH : 0.0;
    sell_ptr->NoPriceLevel = 1;
    snprintf(sell_ptr->UpdateTime, 13, "%s", buy_ptr->UpdateTime);
    snprintf(sell_ptr->LocalTime, 13, "%s", buy_ptr->LocalTime);
    snprintf(sell_ptr->Date, 11, "%s", buy_ptr->Date);
    snprintf(sell_ptr->SecurityID, 7, "%s", buy_ptr->SecurityID);
    snprintf(sell_ptr->PrefixSecurityID, 2, "%s", buy_ptr->PrefixSecurityID);
    snprintf(sell_ptr->Side, 2, "S");
}


template<>
void MarketAdapter::MarketParse<QueueSZ*>(const InsightMarket& market, QueueSZ_t* ptr) 
{
    auto queue = market.mdstock();

    QueueSZ_t buy_ptr = ptr[0];
    buy_ptr->NoOrders = queue.buyorderqueue().size();
    buy_ptr->Volume = buy_ptr->NoOrders >= 1 ? queue.buyorderqtyqueue()[0] : 0;
    buy_ptr->Price = buy_ptr->NoOrders >= 1 ? queue.buypricequeue()[0] * TEN_THOUSANDTH : 0.0;
    buy_ptr->NumOrders = queue.buynumordersqueue().size() ? queue.buynumordersqueue()[0] : 0;
    MARKET_SPREAD(buy_ptr->OrderQty, queue.buyorderqueue(), 50, 1*);
    buy_ptr->NoPriceLevel = 1;
    snprintf(buy_ptr->DataTimeStamp, 13, "%s", TransferTime(queue.mdtime()).c_str());
    snprintf(buy_ptr->LocalTime, 13, "%s", TransferTime(!queue.exchangetime() ? queue.mdtime() : queue.exchangetime()).c_str());
    snprintf(buy_ptr->Date, 11, "%s", TransferDate(queue.mddate()).c_str());
    snprintf(buy_ptr->SecurityID, 7, "%s", queue.htscsecurityid().c_str());
    snprintf(buy_ptr->PrefixSecurityID, 3, "%c%c", buy_ptr->SecurityID[0], buy_ptr->SecurityID[1]);
    snprintf(buy_ptr->Side, 2, "B");
    
    QueueSZ_t sell_ptr = ptr[1];
    sell_ptr->NoOrders = queue.sellorderqueue().size();
    sell_ptr->Price = sell_ptr->NoOrders >= 1 ? queue.sellpricequeue()[0] * TEN_THOUSANDTH : 0.0;
    sell_ptr->Volume = sell_ptr->NoOrders >= 1 ? queue.sellorderqtyqueue()[0] : 0;
    sell_ptr->NumOrders = queue.sellnumordersqueue().size() ? queue.sellnumordersqueue()[0] : 0;
    MARKET_SPREAD(sell_ptr->OrderQty, queue.sellorderqueue(), 50, 1*);
    sell_ptr->NoPriceLevel = 1;
    snprintf(sell_ptr->DataTimeStamp, 13, "%s", buy_ptr->DataTimeStamp);
    snprintf(sell_ptr->LocalTime, 13, "%s", buy_ptr->LocalTime);
    snprintf(sell_ptr->Date, 11, "%s", buy_ptr->Date);
    snprintf(sell_ptr->SecurityID, 7, "%s", buy_ptr->SecurityID);
    snprintf(sell_ptr->PrefixSecurityID, 3, "%s", buy_ptr->PrefixSecurityID);
    snprintf(sell_ptr->Side, 2, "S");
}
