#pragma once

#include "core/datayes_weave/BaseInfo.h"
#include "message_handle.h"
#include "base_define.h"
#include <functional>
#include <map>
#include <stdio.h>

const static double TEN_THOUSANDTH = 0.0001;

// placeholders
const static float PLACE_HOLDER = 0.0;
const static float TINY = 1e-6;

enum AdapterType {
    INSIGHT_MARKET_SH = 0,
    INSIGHT_MARKET_SZ,
    INSIGHT_ORDER_SZ,
    INSIGHT_TRADE_SH,
    INSIGHT_TRADE_SZ,
    INSIGHT_QUEUE_SH,
    INSIGHT_QUEUE_SZ,
    INSIGHT_ORDER_SH,
};


typedef com::htsc::mdc::insight::model::MarketData InsightMarket;

typedef std::function<void(const InsightMarket&, void*)> Adapter;


template<class K, class V>
class Registry
{
public:
    ~Registry() {}
    Registry(const Registry&) = delete;
    Registry &operator = (const Registry&) = delete;

    static Registry* GetInstance() 
    {
        static Registry instance;
        return &instance;
    }

    void Register(const K& type, const V& adapter)
    {
        registry.insert(std::make_pair(static_cast<int>(type), adapter));
    }


    V LookUp(const K& type) 
    {
        V v = nullptr;
        auto iter = registry.find(static_cast<int>(type));
        if (iter != registry.end())
            v = iter->second;
        return v;
    }

    bool View(const K& type)
    {
        if (registry.find(static_cast<int>(type)) != registry.end())
            return true;
        return false;
    }

private:
    Registry() {}

    std::map<int, V> registry;
};

typedef Registry<AdapterType, Adapter> AdapterRegistry;

class MarketAdapter
{
public:
    ~MarketAdapter() {}

    static void RegisterAdapter();
    
    template<typename T>
    static void MarketParse(const InsightMarket&, T*);

private:
    MarketAdapter() {}

};

