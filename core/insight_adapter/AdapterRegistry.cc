#include "core/insight_adapter/AdapterRegistry.h"
#include "core/datayes_weave/MarketSH.h"
#include "core/datayes_weave/MarketSZ.h"
#include "core/datayes_weave/OrderSZ.h"
#include "core/datayes_weave/TradeSH.h"
#include "core/datayes_weave/TradeSZ.h"
#include "core/datayes_weave/QueueSH.h"
#include "core/datayes_weave/QueueSZ.h"
#include "core/datayes_weave/OrderSH.h"


void MarketAdapter::RegisterAdapter() 
{
#define REGISTRY_ADAPTER(K, V) \
    AdapterRegistry::GetInstance()->Register(K, MarketParse<V>) \

//    REGISTRY_ADAPTER(AdapterType::INSIGHT_MARKET_SH, MarketSH);
//    REGISTRY_ADAPTER(AdapterType::INSIGHT_MARKET_SZ, MarketSZ);
//    REGISTRY_ADAPTER(AdapterType::INSIGHT_ORDER_SZ, OrderSZ);
//    REGISTRY_ADAPTER(AdapterType::INSIGHT_TRADE_SH, TradeSH);
//    REGISTRY_ADAPTER(AdapterType::INSIGHT_TRADE_SZ, TradeSZ);
//    REGISTRY_ADAPTER(AdapterType::INSIGHT_QUEUE_SH, QueueSH_t);
//    REGISTRY_ADAPTER(AdapterType::INSIGHT_QUEUE_SZ, QueueSZ_t);
//    REGISTRY_ADAPTER(AdapterType::INSIGHT_ORDER_SH, OrderSZ);
#undef REGISTRY_ADAPTER
}

