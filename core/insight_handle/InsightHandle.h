#pragma once

#include "core/base_function/BaseFunction.h"
#include "base_define.h"
#include "mdc_client_factory.h"
#include "client_interface.h"
#include "message_handle.h"
#include <mutex>
#include <map>

using namespace com::htsc::mdc::gateway;
using namespace com::htsc::mdc::model;
using namespace com::htsc::mdc::insight::model;

class InsightHandle : public MessageHandle {
public:
	InsightHandle(const std::string& forder_name);

	virtual ~InsightHandle() {}

public:
	/**
	* Check whether the subscription is successful after sending the subscription request
	* @param[in] data_stream
	*/
	void OnServiceMessage(const ::com::htsc::mdc::insight::model::MarketDataStream& data_stream);

	/**
	* deal MarketData
	* @param[in] data
	*/
	void OnMarketData(const com::htsc::mdc::insight::model::MarketData& data);

	/**
	* process the backtest data pushed after successful backtest request
    * there risk exists of thread confict when multiple playback tasks are concurrent
	* @param[in] PlaybackPayload backtest
	*/
	void OnPlaybackPayload(const PlaybackPayload& payload);

	/**
	* deal state of backtest
	* @param[in] PlaybackStatus status of backetst
	*/
	void OnPlaybackStatus(const com::htsc::mdc::insight::model::PlaybackStatus& status);

	/**
	* deal result of query request
	* @param[in] MDQueryResponse response of query request
	*/
	void OnQueryResponse(const ::com::htsc::mdc::insight::model::MDQueryResponse& response);

	/**
	* login successfully
	*/
	void OnLoginSuccess();

	/**
	* login failed
	*/
	void OnLoginFailed(int error_no, const std::string& message);

	/**
	* handle cases where all servers are disconnected
	*/
	void OnNoConnections();

	/**
	* when reconnecting
	*/
	void OnReconnect();
	
    /**
	* load config
	*/
	void LoadConfig();


public:
	unsigned ACTIVE = 0;
    int service_value_;
	bool query_exit_;
	bool reconnect_;
	bool login_success_;
	bool no_connections_;
	int reconnect_count_;
	std::string base_forder_; // base dir
	std::mutex task_id_mutex_;
	std::map<std::string, int> task_id_status_;
	long to_storage = 1;
	long to_folder = 0;
};
