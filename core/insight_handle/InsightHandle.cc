#include "core/insight_handle/InsightHandle.h"
#include "core/service_scheduler/ServiceScheduler.h"
#include "core/common_tool/SimpleIni.h"

/**
* construct
*/
InsightHandle::InsightHandle(const std::string& forder_name) :service_value_(0), query_exit_(false), reconnect_(false), 
    login_success_(false), no_connections_(false), reconnect_count_(0), base_forder_(forder_name) {
	// if not exists, then create
	if (!folder_exist(base_forder_.c_str())) {
		debug_print("input folder[%s] not exists!\n", base_forder_.c_str());
		if (!create_folder(base_forder_)) {
			error_print("mkdir base folder[%s] failed! maybe exists!\n", base_forder_.c_str());
		} else {
			debug_print("mkdir base folder[%s] successfully!\n", base_forder_.c_str());
		}
	}
}

/**
* Check whether the subscription is successful after sending the subscription request
* @param[in] data_stream
*/
void InsightHandle::OnServiceMessage(const ::com::htsc::mdc::insight::model::MarketDataStream& data_stream) {
	debug_print("==========> NewHandle: process a Service message");
	service_value_ = 1;
}

/**
* deal MarketData
* @param[in] data
*/
void InsightHandle::OnMarketData(const com::htsc::mdc::insight::model::MarketData& data) {
	static unsigned long count = 0;
	++ACTIVE;
	if (ACTIVE >= 10000) {
        ACTIVE = 0;
        ++count;
	    debug_print("==========> NewHandle: process %d W MarketData message", count);
	}
	// data type
	std::string data_type = get_data_type_name(data.marketdatatype());
	switch (data.marketdatatype()) {
	case MD_TICK:
    case REPLAY_MD_TICK:
    {// snapshot
		if (data.has_mdstock()) {// stock
			// stock type
            if (to_storage)
                ServiceScheduler::GetInstance()->ExecuteMarket(data);
            if (to_folder)
            {
                std::string data_type = get_data_type_name(data.marketdatatype());
			    std::string security_type = get_security_type_name(data.mdstock().securitytype());
			    save_debug_string(base_forder_, data_type, security_type,
				    data.mdstock().htscsecurityid(), data.mdstock().ShortDebugString());
            }
		}
		break;
	}
	case MD_TRANSACTION:
    case REPLAY_MD_TRANSACTION:
    {// transction
		if (data.has_mdtransaction()) {
            if (to_storage)
                ServiceScheduler::GetInstance()->ExecuteTrade(data);
            if (to_folder)
            {
                std::string data_type = get_data_type_name(data.marketdatatype());
			    std::string security_type = get_security_type_name(data.mdtransaction().securitytype());
			    save_debug_string(base_forder_, data_type, security_type,
				    data.mdtransaction().htscsecurityid(), data.mdtransaction().ShortDebugString());
            }
		}
		break;
	}
	case MD_ORDER:
    case REPLAY_MD_ORDER:
    {// order
		if (data.has_mdorder()) {
            if (to_storage)
                ServiceScheduler::GetInstance()->ExecuteOrder(data);
            if (to_folder)
            {
                std::string data_type = get_data_type_name(data.marketdatatype());
			    std::string security_type = get_security_type_name(data.mdorder().securitytype());
			    save_debug_string(base_forder_, data_type, security_type,
				    data.mdorder().htscsecurityid(), data.mdorder().ShortDebugString());
            }
		}
		break;
	}
	case MD_CONSTANT:
	case MD_KLINE_15S:
	case MD_KLINE_1MIN:
	case MD_KLINE_5MIN:
	case MD_KLINE_15MIN:
	case MD_KLINE_30MIN:
	case MD_KLINE_60MIN:
	case MD_KLINE_1D:
	case MD_TWAP_1MIN:
	case MD_VWAP_1MIN:
	case MD_VWAP_1S:
	case AD_FUND_FLOW_ANALYSIS:
	case MD_ETF_BASICINFO:
	case AD_ORDERBOOK_SNAPSHOT:
	default:
		break;
	}
}

/**
* process the backtest data pushed after successful backtest request
* there risk exists of thread confict when multiple playback tasks are concurrent
* @param[in] PlaybackPayload backtest
*/
void InsightHandle::OnPlaybackPayload(const PlaybackPayload& payload) {
	//debug_print("------- PARSE message Playback payload, id:%s", payload.taskid().c_str());
	const MarketDataStream& stream = payload.marketdatastream();
	//debug_print("total number=%d, serial=%d, isfinish=%d", stream.totalnumber(), stream.serial(), stream.isfinished());
	google::protobuf::RepeatedPtrField<MarketData>::const_iterator it
		= stream.marketdatalist().marketdatas().begin();
	while (it != stream.marketdatalist().marketdatas().end()) {
		OnMarketData(*it);
        ++it;
	}
}

/**
* deal state of backtest
* @param[in] PlaybackStatus status of backetst
*/
void InsightHandle::OnPlaybackStatus(const com::htsc::mdc::insight::model::PlaybackStatus& status) {
	//INITIALIZING = 11            // 初始化中
	//PREPARING = 12               // 准备中
	//PREPARED = 13				   // 准备完成
	//RUNNING = 14				   // 运行中
	//APPENDING = 15               // 队列等待中
	//CANCELED = 16                // 已取消
	//COMPLETED = 17               // 已完成
	//FAILED = 18                  // 任务失败
	service_value_ = status.taskstatus();
	//int task_status = status.taskstatus();
	//if (task_status == CANCELED || task_status == COMPLETED || task_status == FAILED)
	//{
	//	task_id_mutex_.lock();
	//	task_id_status_.erase(status.taskid());
	//	task_id_mutex_.unlock();
	//}
}

/**
* deal result of query request
* @param[in] MDQueryResponse response of query request
*/
void InsightHandle::OnQueryResponse(const ::com::htsc::mdc::insight::model::MDQueryResponse& response) {
	if (!response.issuccess()) {
		error_print("query response result: FAIL! ERROR INFO[%d,%s]",
			response.errorcontext().errorcode(), response.errorcontext().message().c_str());
		query_exit_ = true;
		return;
	} else {
		const MarketDataStream& stream = response.marketdatastream();
		debug_print("query response total number=%d, serial=%d, isfinish=%d",
			stream.totalnumber(), stream.serial(), stream.isfinished());
		if (stream.isfinished() == 1) {
			query_exit_ = true;
		}
		google::protobuf::RepeatedPtrField<MarketData>::const_iterator it
			= stream.marketdatalist().marketdatas().begin();
		while (it != stream.marketdatalist().marketdatas().end()) {
			OnMarketData(*it);
			++it;
		}
	}
}

/**
* login successfully
*/
void InsightHandle::OnLoginSuccess() {
	debug_print("-----------------------------");
	debug_print("------- OnLoginSuccess -------");
	login_success_ = true;
	debug_print("-----------------------------");
}

/**
* login failed
*/
void InsightHandle::OnLoginFailed(int error_no, const std::string& message) {
	error_print("-----------------------------");
	error_print("------- OnLoginFailed -------");
	login_success_ = false;
	error_print("------- server reply:%d,%s", error_no, message.c_str());
	error_print("-----------------------------");
}

/**
* handle cases where all servers are disconnected
*/
void InsightHandle::OnNoConnections() {
	error_print("-----------------------------");
	error_print("------- OnNoConnections -----");
	no_connections_ = true;
	reconnect_ = true;
	error_print("-----------------------------");
}

/**
* when reconnecting
*/
void InsightHandle::OnReconnect() {
	error_print("-----------------------------");
	error_print("------- OnReconnect -----");
	++reconnect_count_;
	reconnect_ = true;
	error_print("-----------------------------");
}

void InsightHandle::LoadConfig() {
	
    CSimpleIniA ini;
	SI_Error rc = ini.LoadFile("./config/config.ini");
    if (rc < 0) {
        std::cout << "config file not exists! Use default config instead ..." << std::endl;
        return;
    };

    to_storage =  ini.GetLongValue("HANDLE", "STORAGE", 1);
    to_folder =  ini.GetLongValue("HANDLE", "FOLDER", 0);
    
}
