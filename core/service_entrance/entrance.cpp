#include "core/service_entrance/ServiceEntrance.h"
#include <string>
#include <iostream>

int main(int argc, char** argv) {
	
    static const int MIN = 1;
    static const int MAX = 8;
    int type = 2;
    

    if (argc >= 2 && std::string(argv[1]) == "--test")
    {
        printf("=======================================================================\n");
	    printf("Enter the test mode and select test content\n");
	    printf("1: subscribe_by_all_and_decrease \n");
	    printf("2: subscribe_by_source_type \n");
	    printf("3: subscribe_by_id \n");
	    printf("4: query \n");
	    printf("5: playback \n");
	    printf("6: playback_today \n");
	    printf("7: optimize_today \n");
	    printf("8: playback_recursive \n");
        printf("=======================================================================\n");
        if (argc >= 3 && std::stoi(argv[2]) >= MIN && std::stoi(argv[2]) <= MAX) {
            type = std::stoi(argv[2]);
        } else {
            do {
                std::cin >> type;
                if (type >= MIN && type <= MAX)
                    break;
            } while (true);
        }
    }
	
    //open_file_log();	
	init_env();	
    InsightEntrance insight;
	insight.init();
	
    if (!insight.login()) {
        printf("login failed...\n");
		return 0;
	}
    
	switch (type)
	{
	case 1: {
		insight.subscribe_by_all_and_decrease();
		break;
	}
	case 2: {
		insight.subscribe_by_source_type();
		break;
	}
	case 3: {
		insight.subscribe_by_id();
		break;
	}
	case 4: {
		insight.query_callback();
		break;
	}
	case 5: {
		insight.playback();
		break;
	}
	case 6: {
		insight.playbackToday();
		break;
	}
	case 7: {
		insight.optimizeToday();
		break;
	}
	case 8: {
		insight.playbackRecursive();
		break;
	}
	default:
		break;
	}

	insight.logout();
	fini_env();

    return 0;
}
