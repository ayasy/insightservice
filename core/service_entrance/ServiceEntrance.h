#include "core/insight_handle/InsightHandle.h"
#include "core/base_function/BaseFunction.h"
#include <clickhouse/block.h>


class InsightEntrance{
public:
    /**
    * 初始化配置
    */
    void init();
    
    /**
    * 登录
    * @return 返回值 true 成功 false 失败
    */
    bool login();
    
    /**
    * 退出
    */
    void logout();
    
    /**
    * 订阅全市场证券数据并通过ID剔除部分证券
    * 效率低，不推荐使用
    */
    void subscribe_by_all_and_decrease();
    
    /**
    * 根据证券数据来源订阅行情数据,由三部分确定行情数据
    * 行情源(SecurityIdSource):XSHG(沪市)|XSHE(深市)|...，不填默认全选
    * 证券类型(SecurityType):BondType(债)|StockType(股)|FundType(基)|IndexType(指)|OptionType(期权)|...
    * 数据类型(MarketDataTypes):MD_TICK(快照)|MD_TRANSACTION(逐笔成交)|MD_ORDER(逐笔委托)|...
    */
    void subscribe_by_source_type();
    
    /**
    * 根据证券ID来源订阅行情数据
    */
    void subscribe_by_id();
    
    /**
    * 查询证券的基础信息/最新状态
    * 需要关闭response_callback，response消息通过查询接口参数返回
    */
    void query();
    
    /**
    * 查询证券的基础信息/最新状态
    * 需要打开response_callback，response消息通过回调函数返回
    */
    void query_callback();
    
    /**
    * 进行历史行情回测，单次回测的最大数据量限制请查阅用户使用手册
    */
    void playback();
    
    /**
    * 递归回测补漏
    */
    void playbackRecursive();
    
    
    /**
    * 盘中回放当天数据，使用需要申请权限
    */
    void playbackToday();
    
    /**
    * 盘后优化去重
    * */
    void optimizeToday();

    /**
    * 从配置文件中载入数据库置
    */
    void load_config();

    /**
    * 用于任务挂起
    */
    void hang();
    
private:
    
    std::string user = "MDCBETA2000027";
    std::string password = "hV+.7aSE_xRt";
    std::string ip = "122.96.150.161";
    std::string ip_backup_1;
    std::string ip_backup_2;
    int port = 9242;
    std::string cert_folder = "./cert";
    std::string export_folder = "./export_folder";
    std::string config_path = "./config/config.ini";
    ClientInterface* client = nullptr;
    InsightHandle* handle = nullptr;
    std::string quit_code = "Sin'dorei";

};
