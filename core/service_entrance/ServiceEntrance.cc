#include "core/insight_handle/InsightHandle.h"
#include "core/base_function/BaseFunction.h"
#include "core/service_entrance/ServiceEntrance.h"
#include "core/service_scheduler/ServiceScheduler.h"
#include "core/common_tool/SimpleIni.h"
#include "core/click_house/ClickHouse.h"
#include <fstream>
#include <sys/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>



/**
* 初始化配置
*/
void InsightEntrance::init() {

    load_config();
    ServiceScheduler::GetInstance()->init();
    
    open_trace();//打开trace，输出流量日志消息，默认打开
	open_heartbeat_trace();//打开heartbeat_trace，输出心跳日志消息，默认打开
	open_response_callback();//打开response_callback，自行处理response消息	
	
    //open_cout_log();
	//close_cout_log();
	//close_trace();
	//close_file_log();
	//close_heartbeat_trace();//关闭heartbeat_trace，不输出心跳日志消息
	//close_response_callback();//关闭response_callback，程序自动处理response消息
	
}

/**
* 登录
* @return 返回值 true 成功 false 失败
*/
bool InsightEntrance::login() {
	client = ClientFactory::Instance()->CreateClient(true, cert_folder.c_str());
	if (!client) {
		error_print("create client failed!");
		ClientFactory::Uninstance();
		client = nullptr;
		return false;
	}
	//注册句柄
	handle = new InsightHandle(export_folder);
	client->set_handle_pool_thread_count(10);
	client->RegistHandle(handle);
	//添加备用发现网关地址
	std::vector<std::string> backup_list;
    if (!ip_backup_1.empty()) backup_list.emplace_back(ip_backup_1 + ":" + std::to_string(port));
    if (!ip_backup_2.empty()) backup_list.emplace_back(ip_backup_2 + ":" + std::to_string(port));

    // 开启网关选优
    close_node_auto();
    
    //登录
	int ret = client->LoginByServiceDiscovery(ip, port, user, password, false, backup_list);
	if (ret != 0) {
		error_print("%s", get_error_code_value(ret).c_str());
		ClientFactory::Uninstance();
		client = nullptr;
		DELETE_P(handle);
		return false;
	}
	return true;
}

/**
* 退出
*/
void InsightEntrance::logout() {
    ClientFactory::Uninstance();
    client = nullptr;
	DELETE_P(handle);
    ServiceScheduler::GetInstance()->fint();
}

/**
* 订阅全市场证券数据并通过ID剔除部分证券
* 效率低，不推荐使用
*/
void InsightEntrance::subscribe_by_all_and_decrease() {
	if (client == nullptr || handle == nullptr) {
		error_print("create client failed!");
		return;
	}
	//订阅 SubscribeAll
	ESubscribeActionType action_type = COVERAGE;
	std::unique_ptr<SubscribeAll> subscribe_all(new SubscribeAll());
	subscribe_all->add_marketdatatypes(MD_TICK);
	subscribe_all->add_marketdatatypes(MD_TRANSACTION);
	subscribe_all->add_marketdatatypes(MD_ORDER);
	if ((client->SubscribeAll(action_type, &(*subscribe_all)) < 0)) {
		return;
	}
	//通过DECREASE减少订阅
	action_type = DECREASE;
	std::unique_ptr<SubscribeByID> subscribe_decrease(new SubscribeByID());
	SubscribeByIDDetail* id_detail = subscribe_decrease->add_subscribebyiddetails();
	id_detail->set_htscsecurityid("000002.SZ");
	id_detail->add_marketdatatypes(MD_TICK);
	//订阅
	int ret = client->SubscribeByID(action_type, &(*subscribe_decrease));
	if (ret != 0) {
		error_print("%s", get_error_code_value(ret).c_str());
		return;
	}
    
    hang();
}

/**
* 根据证券数据来源订阅行情数据,由三部分确定行情数据
* 行情源(SecurityIdSource):XSHG(沪市)|XSHE(深市)|...，不填默认全选
* 证券类型(SecurityType):BondType(债)|StockType(股)|FundType(基)|IndexType(指)|OptionType(期权)|...
* 数据类型(MarketDataTypes):MD_TICK(快照)|MD_TRANSACTION(逐笔成交)|MD_ORDER(逐笔委托)|...
*/
void InsightEntrance::subscribe_by_source_type() {
	if (client == nullptr || handle == nullptr) {
		error_print("create client failed!");
		return;
	}
	//订阅 SubscribeBySourceType
	
    // 
    ServiceScheduler::GetInstance()->SetTargetDate(GetCurrentDate());
    ClickHouse::GetInstance()->CreateCurrentMonth();
    ESubscribeActionType action_type = COVERAGE;
	std::unique_ptr<SubscribeBySourceType> source_type(new SubscribeBySourceType());

	//////债券
	//SubscribeBySourceTypeDetail* detail_1 = source_type->add_subscribebysourcetypedetail();
	//SecuritySourceType* security_source_type_1 = new SecuritySourceType();
	//security_source_type_1->set_securityidsource(XSHG);
	//security_source_type_1->set_securitytype(BondType);
	//detail_1->set_allocated_securitysourcetypes(security_source_type_1);
	//detail_1->add_marketdatatypes(MD_TICK);

	//SubscribeBySourceTypeDetail* detail_2 = source_type->add_subscribebysourcetypedetail();
	//SecuritySourceType* security_source_type_2 = new SecuritySourceType();
	//security_source_type_2->set_securityidsource(XSHE);
	//security_source_type_2->set_securitytype(BondType);
	//detail_2->set_allocated_securitysourcetypes(security_source_type_2);
	//detail_2->add_marketdatatypes(MD_TICK);

	////股票
	SubscribeBySourceTypeDetail* detail_3 = source_type->add_subscribebysourcetypedetail();
	SecuritySourceType* security_source_type_3 = new SecuritySourceType();
	security_source_type_3->set_securitytype(StockType);
	security_source_type_3->set_securityidsource(XSHG);
	detail_3->set_allocated_securitysourcetypes(security_source_type_3);
	detail_3->add_marketdatatypes(MD_TICK);
	detail_3->add_marketdatatypes(MD_TRANSACTION);
	detail_3->add_marketdatatypes(MD_ORDER);

	SubscribeBySourceTypeDetail* detail_4 = source_type->add_subscribebysourcetypedetail();
	SecuritySourceType* security_source_type_4 = new SecuritySourceType();
	security_source_type_4->set_securityidsource(XSHE);
	security_source_type_4->set_securitytype(StockType);
	detail_4->set_allocated_securitysourcetypes(security_source_type_4);
	detail_4->add_marketdatatypes(MD_TICK);
	detail_4->add_marketdatatypes(MD_TRANSACTION);
	detail_4->add_marketdatatypes(MD_ORDER);

	//////基金
	//SubscribeBySourceTypeDetail* detail_5 = source_type->add_subscribebysourcetypedetail();
	//SecuritySourceType* security_source_type_5 = new SecuritySourceType();
	//security_source_type_5->set_securityidsource(XSHG);
	//security_source_type_5->set_securitytype(FundType);
	//detail_5->set_allocated_securitysourcetypes(security_source_type_5);
	//detail_5->add_marketdatatypes(MD_TICK);
	////detail_5->add_marketdatatypes(MD_TRANSACTION);
	////detail_5->add_marketdatatypes(MD_ORDER);

	//SubscribeBySourceTypeDetail* detail_6 = source_type->add_subscribebysourcetypedetail();
	//SecuritySourceType* security_source_type_6 = new SecuritySourceType();
	//security_source_type_6->set_securityidsource(XSHE);
	//security_source_type_6->set_securitytype(FundType);
	//detail_6->set_allocated_securitysourcetypes(security_source_type_6);
	//detail_6->add_marketdatatypes(MD_TICK);
	////detail_6->add_marketdatatypes(MD_TRANSACTION);
	////detail_6->add_marketdatatypes(MD_ORDER);

	//////指数
	//SubscribeBySourceTypeDetail* detail_7 = source_type->add_subscribebysourcetypedetail();
	//SecuritySourceType* security_source_type_7 = new SecuritySourceType();
	//security_source_type_7->set_securityidsource(XSHG);
	//security_source_type_7->set_securitytype(IndexType);
	//detail_7->set_allocated_securitysourcetypes(security_source_type_7);
	//detail_7->add_marketdatatypes(MD_TICK);

	//SubscribeBySourceTypeDetail* detail_8 = source_type->add_subscribebysourcetypedetail();
	//SecuritySourceType* security_source_type_8 = new SecuritySourceType();
	//security_source_type_8->set_securityidsource(XSHE);
	//security_source_type_8->set_securitytype(IndexType);
	//detail_8->set_allocated_securitysourcetypes(security_source_type_8);
	//detail_8->add_marketdatatypes(MD_TICK);

	//////期权
	//SubscribeBySourceTypeDetail* detail_9 = source_type->add_subscribebysourcetypedetail();
	//SecuritySourceType* security_source_type_9 = new SecuritySourceType();
	//security_source_type_9->set_securityidsource(XSHG);
	//security_source_type_9->set_securitytype(OptionType);
	//detail_9->set_allocated_securitysourcetypes(security_source_type_9);
	//detail_9->add_marketdatatypes(MD_TICK);

	//SubscribeBySourceTypeDetail* detail_10 = source_type->add_subscribebysourcetypedetail();
	//SecuritySourceType* security_source_type_10 = new SecuritySourceType();
	//security_source_type_10->set_securityidsource(XSHE);
	//security_source_type_10->set_securitytype(OptionType);
	//detail_10->set_allocated_securitysourcetypes(security_source_type_10);
	//detail_10->add_marketdatatypes(MD_TICK);

	////期货
	//SubscribeBySourceTypeDetail* detail_11 = source_type->add_subscribebysourcetypedetail();
	//SecuritySourceType* security_source_type_11 = new SecuritySourceType();
	//security_source_type_11->set_securitytype(FuturesType);
	//detail_11->set_allocated_securitysourcetypes(security_source_type_11);
	//detail_11->add_marketdatatypes(MD_TICK);

	////现货行情
	//SubscribeBySourceTypeDetail* detail_12 = source_type->add_subscribebysourcetypedetail();
	//SecuritySourceType* security_source_type_12 = new SecuritySourceType();
	//security_source_type_12->set_securitytype(SpotType);
	//detail_12->set_allocated_securitysourcetypes(security_source_type_12);
	//detail_12->add_marketdatatypes(MD_TICK);

	////汇率
	//SubscribeBySourceTypeDetail* detail_13 = source_type->add_subscribebysourcetypedetail();
	//SecuritySourceType* security_source_type_13 = new SecuritySourceType();
	//security_source_type_13->set_securitytype(ForexType);
	//detail_13->set_allocated_securitysourcetypes(security_source_type_13);
	//detail_13->add_marketdatatypes();

	//权证
	//SubscribeBySourceTypeDetail* datail_14 = source_type->add_subscribebysourcetypedetail();
	//SecuritySourceType* security_source_type_14 = new SecuritySourceType();
	//security_source_type_14->set_securitytype(WarrantType);
	//security_source_type_14->set_securityidsource(XHKG);
	//datail_14->set_allocated_securitysourcetypes(security_source_type_14);
	//datail_14->add_marketdatatypes(MD_TICK);

	//订阅
	int ret = client->SubscribeBySourceType(action_type, &(*source_type));
	if (ret != 0) {
		error_print("%s", get_error_code_value(ret).c_str());
		return;
	}
    hang();
}

/**
* 根据证券ID来源订阅行情数据
*/
void InsightEntrance::subscribe_by_id() {
	if (client == nullptr || handle == nullptr) {
		error_print("create client failed!");
		return;
	}
	//订阅 SubscribeByID
	ESubscribeActionType action_type = COVERAGE;
	std::unique_ptr<SubscribeByID> id(new SubscribeByID());
	//list1订阅MD_TRANSACTION、MD_ORDER
	std::vector<std::string> securityIdList1;
	securityIdList1.clear();
	securityIdList1.push_back("601168.SH");
	securityIdList1.push_back("000014.SZ");
	securityIdList1.push_back("000015.SZ");
	securityIdList1.push_back("000016.SZ");
	for (unsigned int index = 0; index < securityIdList1.size(); index++) {
		SubscribeByIDDetail* id_detail = id->add_subscribebyiddetails();
		id_detail->set_htscsecurityid(securityIdList1[index]);
		id_detail->add_marketdatatypes(MD_TICK);
		id_detail->add_marketdatatypes(MD_TRANSACTION);
	}
	//list2订阅MD_ORDER
	std::vector<std::string> securityIdList2;
	securityIdList2.clear();
	securityIdList2.push_back("000017.SZ");
	securityIdList2.push_back("000018.SZ");
	securityIdList2.push_back("000019.SZ");
	securityIdList2.push_back("000020.SZ");
	for (unsigned int index = 0; index < securityIdList2.size(); index++) {
		SubscribeByIDDetail* id_detail = id->add_subscribebyiddetails();
		id_detail->set_htscsecurityid(securityIdList2[index]);
		id_detail->add_marketdatatypes(MD_TICK);
		id_detail->add_marketdatatypes(MD_ORDER);
	}
	//add_globalmarketdatatypes会覆盖所有的数据类型设置
	//id->add_globalmarketdatatypes(MD_TRANSACTION)

	//订阅
	int ret = client->SubscribeByID(action_type, &(*id));
	if (ret != 0) {
		error_print("%s", get_error_code_value(ret).c_str());
		return;
	}
	
    hang();
}

/**
* 查询证券的基础信息/最新状态
* 需要关闭response_callback，response消息通过查询接口参数返回
*/
void InsightEntrance::query() {
	if (client == nullptr || handle == nullptr) {
		error_print("create client failed!");
		return;
	}
	//配置查询请求
	std::unique_ptr<MDQueryRequest> request(new MDQueryRequest());
	//设置查询请求类型
	/*
	QUERY_TYPE_BASE_INFORMATION			// 查询历史上所有的指定证券的基础信息(MarketDataTypes = MD_CONSTANT)
	QUERY_TYPE_LATEST_BASE_INFORMATION	// 查询今日最新的指定证券的基础信息(MarketDataTypes = MD_CONSTANT)
	QUERY_TYPE_STATUS					// 查询指定证券的最新一条Tick数据(MarketDataTypes = MD_TICK)
	QUERY_TYPE_ETF_BASE_INFORMATION    //查询ETF的基础信息(MarketDataTypes = MD_ETF_BASICINFO)
	*/
	request->set_querytype(QUERY_TYPE_LATEST_BASE_INFORMATION);
	SecuritySourceType* security_source_type = request->add_securitysourcetype();
	security_source_type->set_securityidsource(XSHE);
	security_source_type->set_securitytype(IndexType);
	request->add_htscsecurityids("601688.SH");

	//接收查询回复
	std::vector<MDQueryResponse*>* responses;
	//发送查询请求并接收回复消息
	int ret = client->RequestMDQuery(&(*request), responses);
	if (ret != 0) {
		error_print("%s", get_error_code_value(ret).c_str());
		return;
	}

	//访问responses，成员如下：
	/*
	int32 queryType;             	     //查询类型
	bool isSuccess;                    // 请求是否成功
	InsightErrorContext errorContext;  // 错误信息
	MarketDataStream marketDataStream; //行情数据包
	*/
	debug_print("query return message count=%d\n\n\n", responses->size());
	//处理查询结果
	for (unsigned int i = 0; i < responses->size(); ++i) {
		if (!responses->at(i)->issuccess()) {
			continue;
		}
		if (!responses->at(i)->has_marketdatastream()) {
			continue;
		}
		//遍历constants
		google::protobuf::RepeatedPtrField<MarketData>::const_iterator it
			= responses->at(i)->marketdatastream().marketdatalist().marketdatas().begin();
		google::protobuf::RepeatedPtrField<MarketData>::const_iterator end
			= responses->at(i)->marketdatastream().marketdatalist().marketdatas().end();
		while (it != end) {
			handle->OnMarketData(*it);
			it++;
		}
	}
	//释放空间
	client->ReleaseQueryResult(responses);
	return;
}

/**
* 查询证券的基础信息/最新状态
* 需要打开response_callback，response消息通过回调函数返回
*/
void InsightEntrance::query_callback() {
	if (client == nullptr || handle == nullptr) {
		error_print("create client failed!");
		return;
	}
	//配置查询请求
	std::unique_ptr<MDQueryRequest> request(new MDQueryRequest());
	//设置查询请求类型
	/*
	QUERY_TYPE_BASE_INFORMATION			// 查询历史上所有的指定证券的基础信息(MarketDataTypes = MD_CONSTANT)
	QUERY_TYPE_LATEST_BASE_INFORMATION	// 查询今日最新的指定证券的基础信息(MarketDataTypes = MD_CONSTANT)
	QUERY_TYPE_STATUS					// 查询指定证券的最新一条Tick数据(MarketDataTypes = MD_TICK)
	QUERY_TYPE_ETF_BASE_INFORMATION    //查询ETF的基础信息(MarketDataTypes = MD_ETF_BASICINFO)
	*/
	//查询601688.SH + 深交所全市场股票的静态信息
	request->set_querytype(QUERY_TYPE_LATEST_BASE_INFORMATION);
	request->add_htscsecurityids("510300.SH");
	SecuritySourceType* source_type1 = request->add_securitysourcetype();
	source_type1->set_securitytype(StockType);
	source_type1->set_securityidsource(XSHE);
	SecuritySourceType* source_type2 = request->add_securitysourcetype();
	source_type2->set_securitytype(StockType);
	source_type2->set_securityidsource(XSHG);

	handle->query_exit_ = false;
	//发送查询请求
	int ret = client->RequestMDQuery(&(*request));
	if (ret != 0) {
		error_print("%s", get_error_code_value(ret).c_str());
		return;
	}
	
	while (true) {
		if (handle->query_exit_) {
			break;
		}
		else{
			msleep(1);
		}		
	}
}


/**
* 进行历史行情回测，单次回测的最大数据量限制请查阅用户使用手册
*/
void InsightEntrance::playback() {
	
    typedef struct PlaybackTask {
        EMarketDataType type;
        int index;
        std::string start;
        std::string end;
    } REPLAY;
    
    if (client == nullptr || handle == nullptr) {
		error_print("create client failed!");
		return;
	}

    std::vector<std::string> SecurityIds;
    std::vector<REPLAY> tasks;
    unsigned BATCH, INTERVAL, ALLOW_MERGE;
   
    auto load_playback_config = [&] {
        CSimpleIniA ini;
	    SI_Error rc = ini.LoadFile("./config/playback.ini");
        if (rc < 0) {
            error_print("playback file not exists! Use default config instead ...");
            return;
        };
        
        BATCH =  ini.GetLongValue("VELOCITY", "BATCH", 20);
        INTERVAL =  ini.GetLongValue("VELOCITY", "INTERVAL", 2000);
        ALLOW_MERGE =  ini.GetLongValue("VELOCITY", "ALLOW_MERGE", 1);

        const char* value;
        long task_num, has_TICK, has_TRANSACTION, has_ORDER, stock_num;
        
        task_num =  ini.GetLongValue("TASKS", "TASK_NUM", 0);
        for (long i = 1; i <= task_num; ++i) {
            REPLAY replay;
            std::string name = "TASK" + std::to_string(i);
            value = ini.GetValue(name.c_str(), "START_TIME", "");
            replay.start = std::string(value);
            value = ini.GetValue(name.c_str(), "END_TIME", "");
            replay.end = std::string(value);
            replay.index = ini.GetLongValue(name.c_str(), "START_INDEX", 0);
        
            has_TICK =  ini.GetLongValue(name.c_str(), "REPLAY_MD_TICK", 0);
            has_TRANSACTION=  ini.GetLongValue(name.c_str(), "REPLAY_MD_TRANSACTION", 0);
            has_ORDER =  ini.GetLongValue(name.c_str(), "REPLAY_MD_ORDER", 0);

            if (ALLOW_MERGE && has_TICK && has_TRANSACTION && has_ORDER) {
                replay.type = REPLAY_MD_TICK_WITH_TRANSACTION_AND_ORDER;
                tasks.emplace_back(replay);
            }
            else {
                if (has_TICK) {
                    replay.type = REPLAY_MD_TICK;
                    tasks.emplace_back(replay);
                }
                if (has_TRANSACTION) {
                    replay.type = REPLAY_MD_TRANSACTION;
                    tasks.emplace_back(replay);
                }
                if (has_ORDER) {
                    replay.type = REPLAY_MD_ORDER;
                    tasks.emplace_back(replay);
                }
            }
        }
        
        stock_num =  ini.GetLongValue("STOCK", "STOCK_NUM", 0);
        for (long i = 1; i <= stock_num; ++i) {
            std::string name = "STOCK_ID" + std::to_string(i);
            value = ini.GetValue("STOCK", name.c_str(), "");
            if (strlen(value))
                SecurityIds.emplace_back(std::string(value));
        }
    };
    
    auto query_securityids = [&SecurityIds](const clickhouse::Block& block) 
    {
        if (block.GetRowCount() == 0)
            return;
        size_t index = 0;
        for (unsigned i = 0; i < block.GetColumnCount(); ++i) {
            if (block.GetColumnName(i) != "SecurityID")
                continue;
            index = i;
            break;
        }

        auto ids = block[index]->As<clickhouse::ColumnFixedString>();

        for (unsigned i = 0; i < ids->Size(); ++i) {
            SecurityIds.emplace_back((*ids)[i]);
        }
    };

    auto do_query = [&] (const std::string& date) {
        SecurityIds.clear();
        std::string query = "select distinct CODE from wind_daily_data.basic_data_dateindex where DATE='" + date.substr(0, 8) + "' order by CODE";
        auto chouse = ClickHouse::GetInstance()->init_client();
        chouse->Select(query, query_securityids);
        debug_print("%d stocks were detected in DAY: %s", SecurityIds.size(), date.substr(0,8).c_str());
    };
    
    
    //auto do_query_beta = [&] (const std::string& date) {
    //    SecurityIds.clear();
    //    std::string sql_date = date.substr(0, 4) + "-" + date.substr(4, 2) + "-" + date.substr(6, 2);
    //    std::string sh_query = "SELECT Distinct SecurityID FROM Level2INSIGHTReplace_MarketData.SH_m" +
    //        date.substr(0, 6) + " where Date='" + sql_date  + "' order by SecurityID";
    //    std::string sz_query = "SELECT Distinct SecurityID FROM Level2INSIGHTReplace_MarketData.SZ_m" +
    //        date.substr(0, 6) + " where Date='" + sql_date  + "' order by SecurityID";
    //    auto chouse = ClickHouse::GetInstance()->init_client();
    //    chouse->Select(sh_query, query_securityids);
    //    chouse->Select(sz_query, query_securityids);
    //    debug_print("%d stocks need to replay in DAY: %s", SecurityIds.size(), date.substr(0,8).c_str());
    //};

    
    //request 需要提前分配空间，并由调用方释放
    auto do_playback = [&] (EMarketDataType type, std::string start, std::string end, unsigned index) {
        std::vector<std::string> ids;
        ids.reserve(BATCH);
        for (; index < SecurityIds.size();) {
            unsigned j = 0;
            for (; index + j < SecurityIds.size() && j < BATCH; ++j) {
                ids.emplace_back(SecurityIds[index+j] + (SecurityIds[index+j][0] == '6' ? ".SH" : ".SZ"));
            }
            index += j;
        
	        int ret = client->RequestPlayback(ids, start, end, type, NO_EXRIGHTS);
	        if (ret != 0) {
	        	error_print("%s", get_error_code_value(ret).c_str());
	        	return;
	        }
	        handle->service_value_ = 0;
            while (1) {
	        	if (handle->service_value_ == CANCELED || handle->service_value_ == COMPLETED || handle->service_value_ == FAILED) {
	        		debug_print("receive playback end message, %d finished...", index);
	        		break;
	        	}
	        	else {
	        		msleep(1000);
	        	}
	        }
            ids.clear();
            msleep(INTERVAL);
        }
        debug_print("EMarketDataType: %d, from: %s, end: %s, jobs done...", int(type), start.c_str(), end.c_str());
    };

    auto do_sync = [this] (int threshold, int limit) {
        unsigned FLAG = 0, OLD_FLAG = 0, count = threshold;
        while (count && --limit) {
            FLAG = handle->ACTIVE;
            if (FLAG == OLD_FLAG) {
                count--;
            } else {
                OLD_FLAG = FLAG;
                count = threshold;
            }
            msleep(1000);
        }
    };

    auto do_create = [tasks] {
        std::set<std::string> months;
        for (auto& replay: tasks) {
            months.insert(replay.start.substr(0, 6));
        }

        for (auto& month: months) {
            ClickHouse::GetInstance()->SetCurrentMonth(month);
            ClickHouse::GetInstance()->CreateCurrentMonth();
            msleep(5);
        }
    };
    
    auto do_optimize = [&] {
        if (tasks.empty())
            return;

        bool if_current_compensate = true;
        std::string current_date = GetCurrentDate();
        for (auto& replay: tasks) {
            if (replay.start.substr(0, 8) != current_date) {
                if_current_compensate = false;
                break;
            }
        }

        if (if_current_compensate)
            optimizeToday();
    };

    load_playback_config();
    do_create();
    for (auto& replay: tasks) 
    {
        debug_print("EMarketDataType: %d, from: %s, end: %s, start_index: %d, jobs working...", 
                    int(replay.type), replay.start.c_str(), replay.end.c_str(), replay.index);
        ClickHouse::GetInstance()->SetCurrentMonth(replay.start);
        do_query(replay.start);
        //do_query_beta(replay.start);
        do_playback(replay.type, replay.start, replay.end, replay.index);
        do_sync(5, 30);
        ServiceScheduler::GetInstance()->flush();
    }

    debug_print("all playback jobs done...");
    msleep(11 * 60 * 1000);
	do_optimize();
    return;
}



/**
* 进行历史行情回测，单次回测的最大数据量限制请查阅用户使用手册
*/
void InsightEntrance::playbackRecursive() {

    typedef struct PlaybackTask {
        EMarketDataType type;
        int index;
        std::string start;
        std::string end;
    } REPLAY;

    if (client == nullptr || handle == nullptr) {
        error_print("create client failed!");
        return;
    }

    std::vector<std::string> SecurityIds;
    std::vector<REPLAY> tasks;

    auto do_query_gamma = [&] {
        std::string basePath = "/home/zhaosheng/WorkSpace/records/";
        std::vector<std::string> result;

        {
            const char* value;

            CSimpleIniA ini;
	        SI_Error rc = ini.LoadFile("./config/playback.ini");
            if (rc < 0) {
                error_print("playback file not exists! Use default config instead ...");
            } else {
                value = ini.GetValue("RECURSIVE", "RECORD_PATH", basePath.c_str());
                basePath = std::string(value);
            }
        }

        {
            DIR *dir;
            struct dirent *ptr;

            if ((dir=opendir(basePath.c_str())) == NULL)
            {
                perror("Open dir error...");
                exit(1);
            }

            while ((ptr=readdir(dir)) != NULL)
            {
                if(strcmp(ptr->d_name,".")==0 || strcmp(ptr->d_name,"..")==0)    ///current dir OR parrent dir
                    continue;
                else if(ptr->d_type == 8) {    ///file
                    result.push_back(std::string(ptr->d_name));
                }
                else if(ptr->d_type == 10) { // link file
                    result.push_back(std::string(ptr->d_name));
                }
            }
            closedir(dir);
        }

        std::map<std::string, std::vector<std::string> > results;
        {
            for (auto fi: result) {

                std::vector<std::string> ids;
                std::string s;
                std::ifstream fin((basePath + fi).c_str());
                fin >> s;
                int len = s.size()/7;
                for (int i = 0; i < len; ++i) {
                    ids.push_back(s.substr(1+i*7, 6));
                }
                results[fi] = ids;
                debug_print("DAY: %s, stock size: %d", fi.c_str(), ids.size());
                ids.clear();

            }

        }
        return results;

    };

   auto do_sync = [this] (int threshold, int limit) {
        unsigned FLAG = 0, OLD_FLAG = 0, count = threshold;
        while (count && --limit) {
            FLAG = handle->ACTIVE;
            if (FLAG == OLD_FLAG) {
                count--;
            } else {
                OLD_FLAG = FLAG;
                count = threshold;
            }
            msleep(1000);
        }
    };

    auto do_optimize = [this, tasks] {
        if (tasks.empty())
            return;

        bool if_current_compensate = true;
        std::string current_date = GetCurrentDate();
        for (auto& replay: tasks) {
            if (replay.start.substr(0, 8) != current_date) {
                if_current_compensate = false;
                break;
            }
        }

        if (if_current_compensate)
            optimizeToday();
    };

    std::map<std::string, std::vector<std::string> > replays = do_query_gamma();

    //request 需要提前分配空间，并由调用方释放
    auto do_playback_gamma = [&] (std::vector<std::string> s_ids, std::string start, std::string end, unsigned index = 0) {
        unsigned BATCH = 50;
        unsigned INTERVAL = 1000;
        std::vector<std::string> ids;
        ids.reserve(BATCH);
        for (; index < s_ids.size();) {
            unsigned j = 0;
            for (; index + j < s_ids.size() && j < BATCH; ++j) {
                ids.emplace_back(s_ids[index+j] + (s_ids[index+j][0] == '6' ? ".SH" : ".SZ"));
            }
            index += j;

            int ret = client->RequestPlayback(ids, start, end, EMarketDataType(103), NO_EXRIGHTS);
            if (ret != 0) {
                error_print("%s", get_error_code_value(ret).c_str());
                return;
            }
            handle->service_value_ = 0;
            while (1) {
                if (handle->service_value_ == CANCELED || handle->service_value_ == COMPLETED || handle->service_value_ == FAILED) {
                    debug_print("receive playback end message, %d finished...", index);
                    break;
                }
                else {
                    msleep(1000);
                }
            }
            ids.clear();
            msleep(INTERVAL);
        }
        debug_print("EMarketDataType: 103, from: %s, end: %s, jobs done...", start.c_str(), end.c_str());
    };


    for (auto replay: replays) {
        std::string start = replay.first + "091000";
        std::string end = replay.first + "153000";
        debug_print("EMarketDataType: 103, from: %s, end: %s, start_index: 0, jobs working...",
                    start.c_str(), end.c_str());
        ClickHouse::GetInstance()->SetCurrentMonth(start);
        do_playback_gamma(replay.second, start, end);
        do_sync(5, 30);
        ServiceScheduler::GetInstance()->flush();
        do_sync(30, 60);

    }

    debug_print("all playback jobs done...");
    msleep(11 * 60 * 1000);
    do_optimize();
    return;
}



/**
* 盘中回放当天数据，使用需要申请权限
*/
void InsightEntrance::playbackToday() {
	if (client == nullptr || handle == nullptr) {
		error_print("create client failed!");
		return;
	}
	std::vector<std::string> idList;
	idList.push_back("601688.SH");
	idList.push_back("000001.SZ");

	int ret = client->RequestPlayback(idList, "20210713093000", "20210713103000", MD_TICK, NO_EXRIGHTS);
	if (ret != 0) {
		error_print("%s", get_error_code_value(ret).c_str());
		return;
	}
	//查询回放状态 EPlaybackTaskStatus
	/*
	INITIALIZING = 11            // 初始化中
	PREPARING = 12               // 准备中
	PREPARED = 13				 // 准备完成
	RUNNING = 14				 // 运行中
	APPENDING = 15               // 队列等待中
	CANCELED = 16                // 已取消
	COMPLETED = 17               // 已完成
	FAILED = 18                  // 任务失败
	*/
	handle->service_value_ = 0;
	while (1) {
		if (handle->service_value_ == CANCELED || handle->service_value_ == COMPLETED || handle->service_value_ == FAILED) {
			debug_print("receive playback end message, quit...");
			break;
		}
		else {
			msleep(1000);
		}
	}
	return;

}

/**
* 盘中优化去重
*/
void InsightEntrance::optimizeToday() {
	if (client == nullptr || handle == nullptr) {
		error_print("create client failed!");
		return;
	}
	
    ClickHouse::GetInstance()->OptimizeToday();
    debug_print("all optimizing jobs done...");

}

void InsightEntrance::hang() 
{
    std::cout << "input quit code to quit..." << std::endl;
    char a[4096];
    do {
        memset(a, 0, 4096);
        std::cin >> a;
        if (std::string(a) == quit_code)
            break;
    } while(true);
    std::cout << "quiting..." << std::endl;

}



void InsightEntrance::load_config() {
	
    CSimpleIniA ini;
	SI_Error rc = ini.LoadFile(config_path.c_str());
    if (rc < 0) {
        error_print("config file not exists! Use default config instead ...");
        return;
    };

    const char* value;
    value = ini.GetValue("INSIGHT", "username", "");
    user = std::string(value);
    value = ini.GetValue("INSIGHT", "password", "");
    password = std::string(value);
    value = ini.GetValue("INSIGHT", "ip", "");
    ip = std::string(value);
    value = ini.GetValue("INSIGHT", "ip_backup_1", "");
    ip_backup_1 = std::string(value);
    value = ini.GetValue("INSIGHT", "ip_backup_2", "");
    ip_backup_2 = std::string(value);
    port = ini.GetLongValue("INSIGHT", "port");
}
