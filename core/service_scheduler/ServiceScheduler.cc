#include "core/service_scheduler/ServiceScheduler.h"
#include "core/click_house/ClickHouse.h"
#include "core/insight_handle/InsightHandle.h"
#include "core/common_tool/SimpleIni.h"
#include "core/common_tool/CommonTool.h"
#include <unistd.h>
#include <time.h>
#include "base_define.h"


void ServiceScheduler::PeriodicWakeUp() 
{
    running = true;

    s_thread = std::thread([this] {
        // wait until the point in ten minutes
        {
            time_t now = time(NULL);
            tm* tm_t = localtime(&now);
            if (tm_t->tm_min % 10 != 0 || tm_t->tm_sec > 1)
                msleep(((10 - tm_t->tm_min % 10) * 60 - tm_t->tm_sec) * 1000);
        }
        while (running)
        {
            flush();
            msleep(INTERVAL);
        }
    });

    if (has_watcher) { 
    	w_thread = std::thread([this] {
    	    // wait until to 09:14:58
    	    //{
    	    //    time_t now = time(NULL);
    	    //    tm* tm_t = localtime(&now);
    	    //    if (tm_t->tm_hour <= 9  && tm_t->tm_min <= 14 && tm_t->tm_sec <= 58)
    	    //        msleep(((9 - tm_t->tm_hour) * 3600 + (14 - tm_t->tm_min) * 60 + 58 - tm_t->tm_sec) * 1000);
    	    //};
	    while (running)
	    {
		    msleep(CONSUME_INTERVAL);
	    	watcher->deliver(nullptr);
	    }
    	    	    
    	});
    }
}

void ServiceScheduler::flush() 
{
   market_sh->deliver(nullptr);
   market_sz->deliver(nullptr);
   order_sz->deliver(nullptr);
   trade_sh->deliver(nullptr);
   trade_sz->deliver(nullptr);
   queue_sh->deliver(nullptr);
   queue_sz->deliver(nullptr);
   order_sh->deliver(nullptr);
}

void ServiceScheduler::FinalWakeUp() 
{
    running = false;
    s_thread.join();
    if (has_watcher) w_thread.join();
}

ServiceScheduler* ServiceScheduler::GetInstance() 
{
    static ServiceScheduler instance;
    return &instance;
}

int ServiceScheduler::init() 
{
    int nRet = -1;
    LoadConfig();
    ClickHouse::GetInstance()->init();

#define INIT_UNIT(TYPE, PTR, SIZE) \
    PTR = new ServiceUnit<TYPE, SIZE>;    \
    if (PTR == nullptr) \
        return nRet;    \
    PTR->run(); \

    INIT_UNIT(MarketSH, market_sh, MARKET_MEM_POOL_SIZE);
    INIT_UNIT(MarketSZ, market_sz, MARKET_MEM_POOL_SIZE);
    INIT_UNIT(OrderSZ, order_sz, ORDER_MEM_POOL_SIZE);
    INIT_UNIT(TradeSH, trade_sh, TRADE_MEM_POOL_SIZE);
    INIT_UNIT(TradeSZ, trade_sz, TRADE_MEM_POOL_SIZE);
    INIT_UNIT(QueueSH, queue_sh, QUEUE_MEM_POOL_SIZE);
    INIT_UNIT(QueueSZ, queue_sz, QUEUE_MEM_POOL_SIZE);
    INIT_UNIT(OrderSH, order_sh, ORDER_MEM_POOL_SIZE);
#undef INIT_UNIT

    if (has_watcher) {
        watcher = new ServiceWatcher;
        if (watcher == nullptr)
            return nRet;
        watcher->run();
    }
    
    PeriodicWakeUp();
    return 0;
}

void ServiceScheduler::fint() 
{
#define FINT_UNIT(PTR) \
    if (PTR) {  \
        PTR->stop(); \
        delete PTR; \
        PTR = nullptr; \
    }

    FINT_UNIT(market_sh);
    FINT_UNIT(market_sz);
    FINT_UNIT(order_sz);
    FINT_UNIT(trade_sh);
    FINT_UNIT(trade_sz);
    FINT_UNIT(queue_sh);
    FINT_UNIT(queue_sz);
    FINT_UNIT(order_sh);
#undef FINT_UNIT

    if (has_watcher && watcher) {
        watcher->stop();
        delete watcher;
        watcher = nullptr;
    }
   
    FinalWakeUp();
}

void ServiceScheduler::ExecuteMarket(const InsightMarket& data)
{
    static unsigned long sh_count = 0;
    static unsigned long sz_count = 0;

    auto date = data.mdstock().mddate();
    if (!JudgeTargetDate(date))
        return;
    
    auto time = data.mdstock().mdtime();
    if (!JudgeActiveTime(time))
        return;

    if (!JudgeStockInfo(data.mdstock().htscsecurityid()))
        return;

    int exchange = data.mdstock().securityidsource();
    if (exchange == 101) {
    	
	sh_count++;
    	if (has_watcher && sh_count >= SAMPLE) {
    	    watcher->CollectTimeStamp(date, time, WatcherTarget::Market_SH_RecievedDelay);
	    sh_count = 0;
    	}

        MarketSH_t market_ptr = nullptr;
        QueueSH_t queue_ptr[2] = {nullptr, nullptr};
        market_ptr = market_sh->apply();
        queue_ptr[0] = queue_sh->apply();
        queue_ptr[1] = queue_sh->apply();
        MarketAdapter::MarketParse<MarketSH>(data, market_ptr);
        market_sh->deliver(market_ptr);
        MarketAdapter::MarketParse<QueueSH_t>(data, queue_ptr);
        queue_sh->deliver(queue_ptr[0]);
        queue_sh->deliver(queue_ptr[1]);
    } else if (exchange == 102) {
    	
	sz_count++;
    	if (has_watcher && sz_count >= SAMPLE) {
    	    watcher->CollectTimeStamp(date, time, WatcherTarget::Market_SZ_RecievedDelay);
	    sz_count = 0;
    	}

        MarketSZ_t market_ptr = nullptr;
        QueueSZ_t queue_ptr[2] = {nullptr, nullptr};
        market_ptr = market_sz->apply();
        queue_ptr[0] = queue_sz->apply();
        queue_ptr[1] = queue_sz->apply();
        MarketAdapter::MarketParse<MarketSZ>(data, market_ptr);
        market_sz->deliver(market_ptr);
        MarketAdapter::MarketParse<QueueSZ_t>(data, queue_ptr);
        queue_sz->deliver(queue_ptr[0]);
        queue_sz->deliver(queue_ptr[1]);
    }
    
}

void ServiceScheduler::ExecuteTrade(const InsightMarket& data)
{

    static unsigned long sh_count = 0;
    static unsigned long sz_count = 0;

    auto date = data.mdtransaction().mddate();
    if (!JudgeTargetDate(date))
        return;
    
    auto time = data.mdtransaction().mdtime();
    if (!JudgeTradeTime(time))
        return;

    if (!JudgeStockInfo(data.mdtransaction().htscsecurityid()))
        return;

   int exchange = data.mdtransaction().securityidsource();
   if (exchange == 101) {

	sh_count++;
    	if (has_watcher && sh_count >= SAMPLE) {
    	    watcher->CollectTimeStamp(date, time, WatcherTarget::Trade_SH_RecievedDelay);
	    sh_count = 0;
    	}

       TradeSH_t trade_ptr = nullptr;
       trade_ptr = trade_sh->apply();
       MarketAdapter::MarketParse<TradeSH>(data, trade_ptr);
       trade_sh->deliver(trade_ptr);
   } else if (exchange == 102) {
    	
       sz_count++;
       if (has_watcher && sz_count >= SAMPLE) {
	   watcher->CollectTimeStamp(date, time, WatcherTarget::Trade_SZ_RecievedDelay);
	   sz_count = 0;
       }

       TradeSZ_t trade_ptr = nullptr;
       trade_ptr = trade_sz->apply();
       MarketAdapter::MarketParse<TradeSZ>(data, trade_ptr);
       trade_sz->deliver(trade_ptr);
   }
    
}

void ServiceScheduler::ExecuteOrder(const InsightMarket& data)
{
    static unsigned long sz_count = 0;
    static unsigned long sh_count = 0;
    
    auto date = data.mdorder().mddate();
    if (!JudgeTargetDate(date))
        return;
    
    auto time = data.mdorder().mdtime();
    if (!JudgeTradeTime(time))
        return;

    if (!JudgeStockInfo(data.mdorder().htscsecurityid()))
        return;

    int exchange = data.mdorder().securityidsource();
    if (exchange == 102) {
        
	sz_count++;
        if (has_watcher && sz_count >= SAMPLE) {
	    watcher->CollectTimeStamp(date, time, WatcherTarget::Order_SZ_RecievedDelay);
	    sz_count = 0;
        }

        OrderSZ_t order_ptr = nullptr;
        order_ptr = order_sz->apply();
        MarketAdapter::MarketParse<OrderSZ>(data, order_ptr);
        order_sz->deliver(order_ptr);
    } else if (exchange == 101) {
    	
	sh_count++;
    	if (has_watcher && sh_count >= SAMPLE) {
    	    watcher->CollectTimeStamp(date, time, WatcherTarget::Order_SH_RecievedDelay);
	    sh_count = 0;
    	}
    	

        OrderSH_t order_ptr = nullptr;
        order_ptr = order_sh->apply();
        MarketAdapter::MarketParse<OrderSH>(data, order_ptr);
        order_sh->deliver(order_ptr);
    }
   
}

void ServiceScheduler::LoadConfig() {
	
    CSimpleIniA ini;
	SI_Error rc = ini.LoadFile("./config/config.ini");
    if (rc < 0) {
        std::cout << "config file not exists! Use default config instead ..." << std::endl;
        return;
    };

    long COEFFICIENT, CLICKHOUSE_INTERVAL;
    has_watcher = ini.GetLongValue("HANDLE", "WATCHER");
    SAMPLE = ini.GetLongValue("WATCHER", "SAMPLE", 10000);
    CONSUME_INTERVAL = ini.GetLongValue("WATCHER", "INTERVAL", 300) * 1000;
    COEFFICIENT =  ini.GetLongValue("TIMER", "COEFFICIENT");
    CLICKHOUSE_INTERVAL =  ini.GetLongValue("TIMER", "CLICKHOUSE_INTERVAL");
    INTERVAL = COEFFICIENT * CLICKHOUSE_INTERVAL;
    
}

void ServiceScheduler::SetTargetDate(const std::string& date) {
    if (date.size() < 8)
        return;
    ClickHouse::GetInstance()->SetCurrentMonth(date);
    has_target = true;
    target_date = date.substr(0, 8);
}

bool ServiceScheduler::JudgeTargetDate(const int32_t& date) {
    if (has_target && std::to_string(date).substr(0, 8) != target_date)
        return false;
    return true;
}

