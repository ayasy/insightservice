#pragma once

#include "core/common_tool/CustQueue.h"
#include "core/common_tool/MemPool.h"
#include "core/click_house/ClickHouse.h"
#include <thread>
#include <typeinfo>

template<class T, std::size_t S>
class ServiceUnit
{
public:
    ServiceUnit() {}
    ~ServiceUnit() {}

    void run()
    {
        static long thousand = 0;
        static int count = 0;
        running = true;
        
        s_thread = std::thread([this] {
            T* node = nullptr;
    	    while (running)
    	    {
    	    	node = pop();
                process(node);
                push(node);
                count++;

                if (count >= 1000) {
                    thousand++, count = 0;
                    debug_print("[%s] has process %ld K message, unit size: %ld Bit, buffer capacity: [%ld/%ld]", 
                                typeid(T).name(), thousand, sizeof(T), s_pool.size(), S);
                }
    	    }
    	});
    }

    void stop()
    {
        running = false;
        s_queue.set(nullptr);
        s_thread.join();
    }

    T* apply()
    {
        T* node = s_pool.get();
    	if (node)
    	    memset(node, 0, sizeof(T));
    	return node;
    }

    void deliver(T* node)
    {   
        s_queue.set(node);
    }

private:
    T* pop()
    {
	    return s_queue.get();
    }

    void push(T* node)
    {
	    s_pool.del(node);
    }

    void process(T* node) {
        if (ClickHouseError::ERROR_SUCCESS != ClickHouse::GetInstance()->UpdateStorage<T>(node)) 
            ClickHouse::GetInstance()->DumpStorage<T>(node);
    }

private:
    CustQueue<T*> s_queue;
	MemPool<T, S> s_pool;
    bool running;
    std::thread s_thread;
};

