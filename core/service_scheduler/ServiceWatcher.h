#pragma once

#include "core/common_tool/CustQueue.h"
#include <vector>
#include <thread>

enum WatcherTarget {
    Market_SH_RecievedDelay = 0,
    Market_SZ_RecievedDelay,
    Trade_SH_RecievedDelay,
    Trade_SZ_RecievedDelay,
    Order_SH_RecievedDelay,
    Order_SZ_RecievedDelay,
    Market_SH_DeliveredDelay = 100,
    Market_SZ_DeliveredDelay,
    Trade_SH_DeliveredDelay,
    Trade_SZ_DeliveredDelay,
    Order_SH_DeliveredDelay,
    Order_SZ_DeliveredDelay,
    WatcherDisplay = 1000,
};

typedef std::pair<long, WatcherTarget> Delay;
typedef std::vector<long> Delays;

class ServiceWatcher 
{
public:
    
    ServiceWatcher() {};
    ~ServiceWatcher() {};

    void run();
    void stop();
    void deliver(std::nullptr_t);
    
    void CollectTimeStamp(const int32_t&, const int32_t&, WatcherTarget);
    static double ComputeMaximum(const Delays&);
    static double ComputeAverage(const Delays&);
    static double ComputeDeviation(const Delays&, const double&);

private:
    
    void process(const Delay&);
    void display();

private:
    Delays market_sh_recieved;
    Delays market_sz_recieved;
    Delays trade_sh_recieved;
    Delays trade_sz_recieved;
    Delays order_sh_recieved;
    Delays order_sz_recieved;
    //Delays market_sh_delivered;
    //Delays market_sz_delivered;
    //Delays trade_sh_delivered;
    //Delays trade_sz_delivered;
    //Delays order_sh_delivered;
    //Delays order_sz_delivered;

    std::thread s_thread;
    CustQueue<Delay> queue;
    bool running;

};

