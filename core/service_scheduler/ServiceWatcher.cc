#include "core/common_tool/CommonTool.h"
#include "core/service_scheduler/ServiceWatcher.h"
#include "core/base_function/BaseFunction.h"
#include <sys/time.h>
#include <algorithm>
#include <functional>
#include <numeric>
#include <cmath>

void ServiceWatcher::run()
{
    running = true;
    s_thread = std::thread([this] {
        Delay node;
        while (running)
        {
            node = queue.get();
            process(node);
        }
    });
}


void ServiceWatcher::stop()
{
    running = false;
    s_thread.join();

}

void ServiceWatcher::deliver(std::nullptr_t ptr) {
    queue.set(std::make_pair(0, WatcherDisplay));
}

void ServiceWatcher::CollectTimeStamp(const int32_t& date,
                                      const int32_t& time,
                                      WatcherTarget target) 
{
    auto compute_delay = [&date, &time] () -> long {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        
        tm tm_;
        int32_t ms = 0;
        tm_.tm_year = date / 10000 - 1900;
        tm_.tm_mon = date / 100 % 100 - 1;
        tm_.tm_mday = date % 100;
        tm_.tm_hour = time / 10000000;
        tm_.tm_min = time / 100000 % 100;
        tm_.tm_sec = time / 1000 % 100;
        tm_.tm_isdst = 0;
        ms  = time % 1000;
        time_t t_ = mktime(&tm_);
        
        long delay = (tv.tv_sec - t_) * 1000 + tv.tv_usec / 1000 - ms;
        return delay;
    };

    long delay = compute_delay();
    if (delay < 0)
        warning_print("Insight time is behind the current time! Target: %d, Date: %ld, Time: %ld, Delay: %ld", 
                      int(target), date, time, delay);
    else {
        queue.set(std::make_pair(delay, target));
        //debug_print("Insight time capture! Target: %d, Date: %ld, Time: %ld, Delay: %ld", 
        //              int(target), date, time, delay);
    }

}

void ServiceWatcher::process(const Delay& delay) 
{
    switch(delay.second) 
    {
        case Market_SH_RecievedDelay:
            market_sh_recieved.emplace_back(delay.first);
            break;
        case Market_SZ_RecievedDelay:
            market_sz_recieved.emplace_back(delay.first);
            break;
        case Trade_SH_RecievedDelay:
            trade_sh_recieved.emplace_back(delay.first);
            break;
        case Trade_SZ_RecievedDelay:
            trade_sz_recieved.emplace_back(delay.first);
            break;
        case Order_SH_RecievedDelay:
            order_sh_recieved.emplace_back(delay.first);
            break;
        case Order_SZ_RecievedDelay:
            order_sz_recieved.emplace_back(delay.first);
            break;
        case WatcherDisplay:
            display();
            break;
	default:
	    break;
    }
}

void ServiceWatcher::display() 
{
    auto do_consume = [this] (Delays& delays, const std::string& flag) 
    {
        if (delays.empty()) return;
        auto maximum = ComputeMaximum(delays);
        auto average = ComputeAverage(delays);
        auto deviation = ComputeDeviation(delays, average);
        debug_print("%s delay has %ld records: [MAXIMUM] = %f, [AVERAGE] = %f, [DEVIATION] = %f", 
                    flag.c_str(), delays.size(), maximum, average, deviation);
        delays.clear();
    };

    do_consume(market_sh_recieved, "Market-SH-Recieved");
    do_consume(market_sz_recieved, "Market-SZ-Recieved");
    do_consume(trade_sh_recieved, "Trade-SH-Recieved");
    do_consume(trade_sz_recieved, "Trade-SZ-Recieved");
    do_consume(order_sh_recieved, "Order-SH-Recieved");
    do_consume(order_sz_recieved, "Order-SZ-Recieved");
}

double ServiceWatcher::ComputeMaximum(const Delays& delays) {
    return static_cast<double>(*std::max_element(delays.begin(), delays.end()));
}

double ServiceWatcher::ComputeAverage(const Delays& delays) {
    long sums = std::accumulate(delays.begin(), delays.end(), 0);
    return static_cast<double>(sums) / delays.size();
}

double ServiceWatcher::ComputeDeviation(const Delays& delays, const double& average) {
    double accum  = 0.0;
    std::for_each (std::begin(delays), std::end(delays), [&](const double d) {
    		accum  += (d - average) * (d - average);
    });

    return sqrt(accum / (delays.size() - 1));
}

