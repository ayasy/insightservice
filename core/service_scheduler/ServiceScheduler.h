#pragma once
#include "core/service_scheduler/ServiceUnit.h"
#include "core/service_scheduler/ServiceWatcher.h"
#include "core/insight_adapter/AdapterRegistry.h"
#include "core/datayes_weave/MarketSH.h"
#include "core/datayes_weave/MarketSZ.h"
#include "core/datayes_weave/OrderSZ.h"
#include "core/datayes_weave/TradeSH.h"
#include "core/datayes_weave/TradeSZ.h"
#include "core/datayes_weave/QueueSH.h"
#include "core/datayes_weave/QueueSZ.h"
#include "core/datayes_weave/OrderSH.h"

static const std::size_t MARKET_MEM_POOL_SIZE = 1024 * 128;
static const std::size_t ORDER_MEM_POOL_SIZE = 1024 * 1024;
static const std::size_t TRADE_MEM_POOL_SIZE = 1024 * 1024;
static const std::size_t QUEUE_MEM_POOL_SIZE = 1024 * 128;


class ServiceScheduler
{
public:
    ~ServiceScheduler() {}
    
    ServiceScheduler(const ServiceScheduler&) = delete;
    ServiceScheduler &operator = (const ServiceScheduler&) = delete;

    static ServiceScheduler* GetInstance();

    void ExecuteMarket(const InsightMarket&);
    void ExecuteTrade(const InsightMarket&);
    void ExecuteOrder(const InsightMarket&);

    int init();
    void fint();
    void flush();

    void SetTargetDate(const std::string&);


private:
    ServiceScheduler() {}
    void LoadConfig();
    void PeriodicWakeUp();
    void FinalWakeUp();
    bool JudgeTargetDate(const int32_t&);

private:
    ServiceUnit<MarketSH, MARKET_MEM_POOL_SIZE> *market_sh;
    ServiceUnit<MarketSZ, MARKET_MEM_POOL_SIZE> *market_sz;
    ServiceUnit<OrderSZ, ORDER_MEM_POOL_SIZE> *order_sz;
    ServiceUnit<TradeSH, TRADE_MEM_POOL_SIZE> *trade_sh;
    ServiceUnit<TradeSZ, TRADE_MEM_POOL_SIZE> *trade_sz;
    ServiceUnit<QueueSH, QUEUE_MEM_POOL_SIZE> *queue_sh;
    ServiceUnit<QueueSZ, QUEUE_MEM_POOL_SIZE> *queue_sz;
    ServiceUnit<OrderSH, ORDER_MEM_POOL_SIZE> *order_sh;
    
    ServiceWatcher* watcher;

    bool running;
    std::thread s_thread;
    std::thread w_thread;
    unsigned INTERVAL;
    unsigned SAMPLE;
    unsigned CONSUME_INTERVAL;
    bool has_target = false;
    bool has_watcher = false;
    std::string target_date;
};

